;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01014B7B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
;Load the Protocol

(OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex = 0
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property OSM_AlternateStart_Quest Auto Const

Quest Property OSM_101 Auto Const
