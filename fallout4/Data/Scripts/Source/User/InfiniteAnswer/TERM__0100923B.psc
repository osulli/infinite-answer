;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:TERM__0100923B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 0
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 1
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 2
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 3
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 4
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
(OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).ChosenEquipment = 5
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property OSM_AlternateStart_Quest Auto Const
