Scriptname InfiniteAnswer:EnterEquipmentRoomTrigger extends ObjectReference 

ObjectReference Property DoorToNextAreaCloseTrigger Auto Const

ObjectReference Property PreviousAreDoor Auto Const
Bool Property TriggerEntered Auto
Actor Property PlayerRef Auto


Event OnTriggerEnter(ObjectReference akActionRef)

If TriggerEntered == false
	TriggerEntered = true
  PreviousAreDoor.enable()

  PlayerRef = Game.GetPlayer()


		IF (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 1
		ProtocolLocker.additem(OSM_GoodneighborGhoul)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 2
			ProtocolLocker.additem(OSM_ChildOfAtomGhoul)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 3
			ProtocolLocker.additem(OSM_2287)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 4
			ProtocolLocker.additem(OSM_PowerPlay)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 5
			ProtocolLocker.additem(OSM_AtomsForever)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 6
			ProtocolLocker.additem(OSM_SteelChains)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 7
			ProtocolLocker.additem(OSM_Shipwrecked)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 8
			ProtocolLocker.additem(OSM_GunningToTheTop)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 9
			ProtocolLocker.additem(OSM_BrownsRequiem)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 10
			ProtocolLocker.additem(OSM_MercenaryForHire)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 11
			ProtocolLocker.additem(OSM_NathanTheScav)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 12
					
					;male female check
					ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
					if (PlayerBase.GetSex() == 0) ;male
					ProtocolLocker.additem(OSM_CutthroatFashionMale)
					elseif (PlayerBase.GetSex() == 1) ;female
					ProtocolLocker.additem(OSM_CutthroatFashionFemale)	
					endIf		

			
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 13
			ProtocolLocker.additem(OSM_RaiderScum)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 14
			ProtocolLocker.additem(OSM_DangerousCriminal)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 15
			ProtocolLocker.additem(OSM_Addict)
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 16
			ProtocolLocker.additem(OSM_EnclaveRemnantSoldier)

			 		;EnclaveOfficerUniform
 					bool EnclaveOfficerUniformInstalled = Game.IsPluginInstalled("Enclave Officer.esp")
					if EnclaveOfficerUniformInstalled == true
					Armor EnclaveOfficerUniform = Game.GetFormFromFile(0x000800, "Enclave Officer.esp") as Armor
     		 		ProtocolLocker.Additem(EnclaveOfficerUniform, 1, false) 						
     		 		EndIf
     		 		
					;EnclaveX02
					bool EnclaveX02Installed = Game.IsPluginInstalled("EnclaveX02.esp")
					if EnclaveX02Installed == true
					
					Armor EnclaveX021 = Game.GetFormFromFile(0x000800, "EnclaveX02.esp") as Armor
					Armor EnclaveX022 = Game.GetFormFromFile(0x000801, "EnclaveX02.esp") as Armor
					Armor EnclaveX023 = Game.GetFormFromFile(0x000802, "EnclaveX02.esp") as Armor
					Armor EnclaveX024 = Game.GetFormFromFile(0x000803, "EnclaveX02.esp") as Armor
					Armor EnclaveX025 = Game.GetFormFromFile(0x000804, "EnclaveX02.esp") as Armor
					Armor EnclaveX026 = Game.GetFormFromFile(0x000805, "EnclaveX02.esp") as Armor

					PlayerPowerArmor.Additem(EnclaveX021, 1, false)
					PlayerPowerArmor.Additem(EnclaveX022, 1, false)
					PlayerPowerArmor.Additem(EnclaveX023, 1, false)
					PlayerPowerArmor.Additem(EnclaveX024, 1, false)
					PlayerPowerArmor.Additem(EnclaveX025, 1, false)
					PlayerPowerArmor.Additem(EnclaveX026, 1, false)
					endif
		
		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 17
			ProtocolLocker.additem(OSM_EnclaveRemnantScientist)

			 		;EnclaveOfficerUniform
 					bool EnclaveOfficerUniformInstalled = Game.IsPluginInstalled("Enclave Officer.esp")
					if EnclaveOfficerUniformInstalled == true
					Armor EnclaveOfficerUniform = Game.GetFormFromFile(0x000804, "Enclave Officer.esp") as Armor
     		 		ProtocolLocker.Additem(EnclaveOfficerUniform, 1, false)
     		 		EndIf

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 18
					
					ProtocolLocker.additem(OSM_NCRRangerVeteran)

			 		;NCR Ranger
 					bool NCRRangerVeteranArmorInstalled = Game.IsPluginInstalled("Rangergearnew.esp")
					if NCRRangerVeteranArmorInstalled == true
					Armor NCRRangerVeteran1 = Game.GetFormFromFile(0x000804, "Rangergearnew.esp") as Armor
					Armor NCRRangerVeteran2 = Game.GetFormFromFile(0x000805, "Rangergearnew.esp") as Armor
					Armor NCRRangerVeteran3 = Game.GetFormFromFile(0x000806, "Rangergearnew.esp") as Armor
     		 		ProtocolLocker.Additem(NCRRangerVeteran1, 1, false)
     		 		ProtocolLocker.Additem(NCRRangerVeteran2, 1, false)
     		 		ProtocolLocker.Additem(NCRRangerVeteran3, 1, false)
					endif	

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 19
			ProtocolLocker.additem(OSM_SHIAgent)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 20
			ProtocolLocker.additem(OSM_InstituteSafehouseScientist)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 21
			ProtocolLocker.additem(OSM_BoSKnight)		

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 22
			ProtocolLocker.additem(OSM_BosPaladin)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 23
			ProtocolLocker.additem(OSM_RailroadInitiate)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 24
			ProtocolLocker.additem(OSM_MinutemenGeneral)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 25
			ProtocolLocker.additem(OSM_Cannibal)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 26
			ProtocolLocker.additem(OSM_Courser)	

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 27
			ProtocolLocker.additem(OSM_InstituteAdvSciScientist)	

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 28
			ProtocolLocker.additem(OSM_Survivalist)	

			 		;Rebel
 					bool RebelArmorInstalled = Game.IsPluginInstalled("The Rebel.esp")
					if RebelArmorInstalled == true
					Armor Rebel1 = Game.GetFormFromFile(0x000840, "The Rebel.esp") as Armor
					Armor Rebel2 = Game.GetFormFromFile(0x000841, "The Rebel.esp") as Armor
					Armor Rebel3 = Game.GetFormFromFile(0x000842, "The Rebel.esp") as Armor
					Armor Rebel4 = Game.GetFormFromFile(0x000848, "The Rebel.esp") as Armor
					Armor Rebel5 = Game.GetFormFromFile(0x00084B, "The Rebel.esp") as Armor
     		 		ProtocolLocker.Additem(Rebel1, 1, false)
     		 		ProtocolLocker.Additem(Rebel2, 1, false)
     		 		ProtocolLocker.Additem(Rebel3, 1, false)
     		 		ProtocolLocker.Additem(Rebel4, 1, false)
     		 		ProtocolLocker.Additem(Rebel5, 1, false)
					endif	


		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 29
			ProtocolLocker.additem(OSM_Tinkerer)	

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 30
			ProtocolLocker.additem(OSM_Gen2SynthEscapee)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 31
			ProtocolLocker.additem(OSM_Vault81Villain)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 32
			ProtocolLocker.additem(OSM_NomadTrader)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 33
			ProtocolLocker.additem(OSM_Purger)

		elseif (OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex == 34
			ProtocolLocker.additem(OSM_GettingRusty)

			bool AutomatronInstalled = Game.IsPluginInstalled("DLCRobot.esm")
			if AutomatronInstalled == true

			Weapon MrHandyBuzzBlade = Game.GetFormFromFile(0x0020f8, "DLCRobot.esm") as Weapon
			Potion RobotRepairKit = Game.GetFormFromFile(0x004f12, "DLCRobot.esm") as Potion
			Armor RobotArmor1 = Game.GetFormFromFile(0x00863f, "DLCRobot.esm") as Armor
			Armor RobotArmor2 = Game.GetFormFromFile(0x008646, "DLCRobot.esm") as Armor
			Armor RobotArmor3 = Game.GetFormFromFile(0x008648, "DLCRobot.esm") as Armor
			Armor RobotArmor4 = Game.GetFormFromFile(0x008642, "DLCRobot.esm") as Armor
			Armor RobotArmor5 = Game.GetFormFromFile(0x008644, "DLCRobot.esm") as Armor
			Faction RustDevils = Game.GetFormFromFile(0x004385, "DLCRobot.esm") as Faction
			MiscObject RobotWorkbenchSchematics = Game.GetFormFromFile(0x00254a, "DLCRobot.esm") as MiscObject

			ProtocolLocker.Additem(MrHandyBuzzBlade, 1, false)
			ProtocolLocker.Additem(RobotRepairKit, 3, false)
			ProtocolLocker.Additem(RobotArmor1, 1, false)
			ProtocolLocker.Additem(RobotArmor2, 1, false)
			ProtocolLocker.Additem(RobotArmor3, 1, false)
			ProtocolLocker.Additem(RobotArmor4, 1, false)
			ProtocolLocker.Additem(RobotArmor5, 1, false)
			ProtocolLocker.Additem(RobotWorkbenchSchematics, 1, false)
			PlayerRef.AddToFaction(RustDevils)
			endIf















		;;;;;
		EndIf ;Protocol Check



;BulletTime
bool BulletTimeInstalled = Game.IsPluginInstalled("SlowTime.esp")
if BulletTimeInstalled == true
Potion BulletTimeConfig = Game.GetFormFromFile(0x000811, "SlowTime.esp") as Potion
Potion BulletTimeTrigger = Game.GetFormFromFile(0x000803, "SlowTime.esp") as Potion
Game.GetPlayer().Additem(BulletTimeConfig, 1, false)
Game.GetPlayer().Additem(BulletTimeTrigger, 1, false)
endif


;Don'tCallMeSettler
bool DontCallMeSettlerInstalled = Game.IsPluginInstalled("SettlersRename.esp")
if DontCallMeSettlerInstalled == true
Holotape DontCallMeSettlerConfig = Game.GetFormFromFile(0x000921, "SettlersRename.esp") as Holotape
Game.GetPlayer().Additem(DontCallMeSettlerConfig, 1, false)
endif

;SurvivalSaving by DeEz
bool SurvivalSavingInstalled = Game.IsPluginInstalled("SurvivalSaving_Holotapes.esp")
if SurvivalSavingInstalled == true
Holotape SurvivalSavingConfig = Game.GetFormFromFile(0x000f99, "SurvivalSaving_Holotapes.esp") as Holotape
Game.GetPlayer().Additem(SurvivalSavingConfig, 1, false)
endif


If Game.GetDifficulty() == 6 ;survival
		SurvivalBoost.enable()
EndIf




RailroadClothingArmorModAvailable.SetValue(1) 
Utility.Wait(0.1)        
Game.ShowSPECIALMenu() 
EndIf ;boolTriggerEntered {so it only happens once}		
EndEvent


Group ProtocolLocker
ObjectReference Property ProtocolLocker Auto Const

LeveledItem Property OSM_GoodneighborGhoul Auto Const
LeveledItem Property OSM_ChildOfAtomGhoul Auto Const
LeveledItem Property OSM_2287 Auto Const
LeveledItem Property OSM_PowerPlay Auto Const
LeveledItem Property OSM_AtomsForever Auto Const
LeveledItem Property OSM_SteelChains Auto Const
LeveledItem Property OSM_Shipwrecked Auto Const
LeveledItem Property OSM_GunningToTheTop Auto Const
LeveledItem Property OSM_MercenaryForHire Auto Const
LeveledItem Property OSM_NathanTheScav Auto Const
LeveledItem Property OSM_CutthroatFashionMale Auto Const
LeveledItem Property OSM_CutthroatFashionFemale Auto Const
LeveledItem Property OSM_RaiderScum Auto Const
LeveledItem Property OSM_DangerousCriminal Auto Const
LeveledItem Property OSM_Addict Auto Const
LeveledItem Property OSM_EnclaveRemnantSoldier Auto Const
LeveledItem Property OSM_EnclaveRemnantScientist Auto Const
LeveledItem Property OSM_NCRRangerVeteran Auto Const
LeveledItem Property OSM_SHIAgent Auto Const
LeveledItem Property OSM_BrownsRequiem Auto Const
LeveledItem Property OSM_InstituteSafehouseScientist Auto Const
LeveledItem Property OSM_BoSKnight Auto Const
LeveledItem Property OSM_BosPaladin Auto Const
LeveledItem Property OSM_RailroadInitiate Auto Const
LeveledItem Property OSM_MinutemenGeneral Auto Const
LeveledItem Property OSM_GettingRusty Auto Const
LeveledItem Property OSM_Purger Auto Const
LeveledItem Property OSM_NomadTrader Auto Const
LeveledItem Property OSM_Vault81Villain Auto Const
LeveledItem Property OSM_Gen2SynthEscapee Auto Const
LeveledItem Property OSM_Tinkerer Auto Const
LeveledItem Property OSM_Cannibal Auto Const
LeveledItem Property OSM_Courser Auto Const
LeveledItem Property OSM_InstituteAdvSciScientist Auto Const
LeveledItem Property OSM_Survivalist Auto Const

endGroup

Quest Property OSM_101 Auto Const
GlobalVariable Property RailroadClothingArmorModAvailable Auto

ObjectReference Property WorkshopContainer Auto Const

ObjectReference Property PlayerPowerArmor Auto Const

ObjectReference Property SurvivalBoost Auto Const
