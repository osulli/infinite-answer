Scriptname InfiniteAnswer:ExitASCell extends ObjectReference Const


Quest Property OSM_AlternateStart_Quest Auto Const



Event OnOpen(ObjectReference akActionRef)
  if (OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).OSM_UsingCustomProtocol == 0.0000
    (OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).LoadPresetProtocol()
    OSM_101.SetStage(30)
    
  ElseIf (OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).OSM_UsingCustomProtocol == 1.0000
    (OSM_AlternateStart_Quest as InfiniteAnswer:OSM_LoadProtocol).LoadCustomProtocol()
    OSM_101.SetStage(1000)
  endIf
endEvent


Quest Property OSM_101 Auto Const
