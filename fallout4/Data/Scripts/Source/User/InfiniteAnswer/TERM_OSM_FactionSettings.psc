;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:TERM_OSM_FactionSettings Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(MinutemenFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(OSM_MinutemenEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
;Remove from MinutemenFaction AND MinutemenEnemyFaction

   Game.GetPlayer().RemoveFromFaction(MinutemenFaction)
   Game.GetPlayer().RemoveFromFaction(OSM_MinutemenEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(BrotherhoodofSteelFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(OSM_BrotherhoodofSteelEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveFromFaction(BrotherhoodofSteelFaction)
   Game.GetPlayer().RemoveFromFaction(OSM_BrotherhoodofSteelEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
;vanilla

   Game.GetPlayer().RemoveFromFaction(MinutemenFaction)
   Game.GetPlayer().RemoveFromFaction(RailroadFaction)
   Game.GetPlayer().RemoveFromFaction(InstituteFaction)
   Game.GetPlayer().RemoveFromFaction(RaiderFaction)
   Game.GetPlayer().RemoveFromFaction(GunnerFaction) 
   Game.GetPlayer().RemoveFromFaction(BrotherhoodofSteelFaction)
   Game.GetPlayer().RemoveFromFaction(TheForgedFaction)
   Game.GetPlayer().RemoveFromFaction(GoodneighborGenericNPCFaction)
   Game.GetPlayer().RemoveFromFaction(REScavengerFaction)
   Game.GetPlayer().RemoveFromFaction(ChildrenOfAtomFaction)
   Game.GetPlayer().RemoveFromFaction(DN054AtomCatFaction)
   Game.GetPlayer().RemoveFromFaction(OSM_BrotherhoodofSteelEnemy)
   Game.GetPlayer().RemoveFromFaction(OSM_ForgedEnemy)
   Game.GetPlayer().RemoveFromFaction(OSM_GunnerEnemy)
   Game.GetPlayer().RemoveFromFaction(OSM_InstituteEnemy)
   Game.GetPlayer().RemoveFromFaction(OSM_MinutemenEnemy) 
   Game.GetPlayer().RemoveFromFaction(OSM_RaiderEnemy)
   Game.GetPlayer().RemoveFromFaction(OSM_RailroadEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(InstituteFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(OSM_InstituteEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveFromFaction(InstituteFaction)
   Game.GetPlayer().RemoveFromFaction(OSM_InstituteEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(RailroadFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(OSM_RailroadEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveFromFaction(OSM_RailroadEnemy) 
   Game.GetPlayer().RemoveFromFaction(RailroadFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(RaiderFaction)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddToFaction(OSM_RaiderEnemy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveFromFaction(RaiderFaction)
   Game.GetPlayer().RemoveFromFaction(OSM_RaiderEnemy)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment



















         Group Factions

         Faction Property OSM_BrotherhoodofSteelEnemy Auto Const
         Faction Property OSM_ForgedEnemy Auto Const
         Faction Property OSM_GunnerEnemy Auto Const
         Faction Property OSM_InstituteEnemy Auto Const
         Faction Property OSM_MinutemenEnemy Auto Const
         Faction Property OSM_RaiderEnemy Auto Const
         Faction Property OSM_RailroadEnemy Auto Const

         Faction Property BrotherhoodofSteelFaction Auto Const
         Faction Property TheForgedFaction Auto Const
         Faction Property GunnerFaction Auto Const
         Faction Property InstituteFaction Auto Const
         Faction Property MinutemenFaction Auto Const
         Faction Property RaiderFaction Auto Const
         Faction Property RailroadFaction Auto Const
         Faction Property CZ_Faction Auto Const

         Faction Property GoodneighborGenericNPCFaction Auto Const
         Faction Property REScavengerFaction Auto Const
         Faction Property ChildrenOfAtomFaction Auto Const
         Faction Property DN054AtomCatFaction Auto Const
         endGroup

         Group Races
         Race Property GhoulRace Auto Const
         Race Property HumanRace Auto Const
         Race Property SynthGen1Race Auto Const
         Race Property SynthGen2Race Auto Const
         endGroup
