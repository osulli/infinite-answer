Scriptname InfiniteAnswer:OSM_LoadProtocol extends Quest



Function LoadPresetProtocol()

PlayerRef = Game.GetPlayer()

      OSM_CustomProtocolIndex.SetValue(ProtocolIndex) 




      If ProtocolIndex == 0 ;if player is stupid and doesnt choose a protocol make them a tramp cos thats what they are lmfao
         PlayerRef.MoveTo(l_DiamondCityMarket)   
         PlayerRef.Setrace(HumanRace)   
         PlayerRef.Additem(Jet, 17 , true)
         PlayerRef.Additem(Switchblade, 1 , true) 
         PlayerRef.EquipItem(Switchblade,  false, true)
         PlayerRef.Additem(ClothesResident6, 1 , true) ; Wastelander Light
         PlayerRef.EquipItem(ClothesResident6,  false, true)  ;  
         PlayerRef.ModValue(AddictionJet, 50) ;

      
      elseif ProtocolIndex ==  1    ; Goodneighbour Ghoul
      
       PlayerRef.MoveTo(NewPropertya) 
             ;PlayerRef.MoveTo(l_Goodneighbour)   ; Goodneighbour
             PlayerRef.Setrace(GhoulRace)   ; Ghoul
   
             PlayerRef.ModValue(Endurance, 6 ) ; Endurance
             PlayerRef.ModValue(Charisma, 2 ) ; Charisma


             PlayerRef.ModValue(AddictionJet, 20 ) ; JetAddiction

             PlayerRef.AddPerk(Ghoulish01)   
             PlayerRef.AddPerk(Ghoulish02)   
             PlayerRef.AddPerk(Ghoulish03)   
             PlayerRef.AddPerk(RadResistant01)   
             PlayerRef.AddPerk(RadResistant02)   
             PlayerRef.AddPerk(RadResistant03)   

       
      elseif ProtocolIndex ==  2    ; Church Of Atom
            PlayerRef.MoveTo(l_GlowingSeaCrater)   ; GlowingSeaCrater
            PlayerRef.Setrace(GhoulRace)   ; Ghoul


            PlayerRef.ModValue(Endurance, 6 ) ; Endurance
            PlayerRef.ModValue(Charisma, 4 ) ; Charisma


            PlayerRef.AddPerk(Ghoulish01)   
            PlayerRef.AddPerk(Ghoulish02)   
            PlayerRef.AddPerk(Ghoulish03)   
            PlayerRef.AddPerk(RadResistant01)   
            PlayerRef.AddPerk(RadResistant02)   
            PlayerRef.AddPerk(RadResistant03)   

       
      elseif ProtocolIndex ==  3    ; 2287
            PlayerRef.MoveTo(l_Vault111Ext)   ; Vault111 Exterior
            PlayerRef.Setrace(HumanRace)   
 


       

      elseif ProtocolIndex ==  4    ; Power Play

         PlayerRef.MoveTo(l_Concord)   
         PlayerRef.Setrace(HumanRace)   

         ;;;PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)   ; BoS Enemy/ others neutral
         BoSDialoguePrydwen.setstage(40) ;Kicked from BoS faction
       
      elseif ProtocolIndex ==  5    ; Atoms Forever
            
         PlayerRef.MoveTo(l_WarwickHomestead)   
         PlayerRef.Setrace(HumanRace)   

         PlayerRef.ModValue(Strength, 3 )



      elseif ProtocolIndex ==  6    ; Steel Chains

      	PlayerRef.MoveTo(l_CambridgePD) 
         PlayerRef.ModValue(Strength, 3 ) ; Strength
         PlayerRef.ModValue(Endurance, 3 ) ; Endurance

         BoS101.SetStage(0) ;skip Fire Support, launch into Call To Arms
         BoSDialoguePrydwen.SetStage(10) ;set rank to initiate

         PlayerRef.AddToFaction(BrotherhoodofSteelFaction)   ; BoS Ally



      elseif ProtocolIndex ==  7    ; Shipwrecked

         PlayerRef.MoveTo(l_Shipwreck)   ; Shipwreck
         PlayerRef.Setrace(HumanRace)   


         PlayerRef.ModValue(Endurance, 3 )

         PlayerRef.AddPerk(LoneWanderer01)

         ActorBase PlayerBase = PlayerRef.GetBaseObject() as ActorBase
         if (PlayerBase.GetSex() == 0) ;male
         PlayerRef.AddPerk(Aquaboy01) ;Aquaboy01
         elseif (PlayerBase.GetSex() == 1) ;female
         PlayerRef.AddPerk(Aquagirl01) ;Aquagirl01
         endIf       


      elseif ProtocolIndex ==  8    ; Gunning To The Top
 
         PlayerRef.MoveTo(l_FiddlersGreen)   ; Fiddler's Green
         PlayerRef.Setrace(HumanRace)   


         PlayerRef.AddToFaction(GunnerFaction)   ; Gunner Ally

      elseif ProtocolIndex ==  9    ; Brown's Requiem

         PlayerRef.MoveTo(l_ValentinesDetectiveAgency)   
         PlayerRef.Setrace(HumanRace)  

         PlayerRef.ModValue(Perception, 7 ) ; Perception
         PlayerRef.ModValue(Charisma, 6 ) ; Charisma
         PlayerRef.ModValue(Intelligence, 6 ) ; Intelligence


         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)   ;
         PlayerRef.AddToFaction(OSM_InstituteEnemy)
         PlayerRef.AddToFaction(OSM_ForgedEnemy) 
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_RaiderEnemy)

         PlayerRef.AddPerk(Locksmith01)
         PlayerRef.AddPerk(Hacker01)
         PlayerRef.AddPerk(Sneak01)

         MS07.SetStage(2) ;MS07


      elseif ProtocolIndex ==  10    ; Mercenary for hire

         PlayerRef.MoveTo(l_DiamondCityMarket)   
         PlayerRef.Setrace(HumanRace)   


      elseif ProtocolIndex ==  11    ; Nathan The Scav

         PlayerRef.MoveTo(l_LibertaliaNorth)   ; Crashed plane

         PlayerRef.Setrace(HumanRace)   

   
         PlayerRef.ModValue(Strength, 2 ) ; Strength
         PlayerRef.ModValue(Perception, 6 ) ; Perception
         PlayerRef.ModValue(Endurance, 2 ) ; Endurance

   
         PlayerRef.AddToFaction(REScavengerFaction)   ; Scavenger Ally

         PlayerRef.AddPerk(StrongBack01)   


      elseif ProtocolIndex ==  12    ; Cutthroat Fashion

         PlayerRef.MoveTo(l_DiamondCityMarket)   
         PlayerRef.Setrace(HumanRace)

         ;custom script ;male female check
         ActorBase PlayerBase = PlayerRef.GetBaseObject() as ActorBase
         if (PlayerBase.GetSex() == 0) ;male
 
         PlayerRef.AddPerk(LadyKiller01)
         elseif (PlayerBase.GetSex() == 1) ;female
 
         PlayerRef.AddPerk(BlackWidow01)
         endIf  
   

         PlayerRef.ModValue(Charisma, 4 ) ; Charisma

   
         PlayerRef.AddPerk(Armorer01)   ; Armorer01
         PlayerRef.AddPerk(LocalLeader01)   ; LocalLeader01
         PlayerRef.AddPerk(MisterSandman01) 
          

      elseif ProtocolIndex ==  13    ; Raider Scum
 
         PlayerRef.MoveTo(l_CorvegaAssemblyPlant)   ; Corvega/Libertalia/Dunwich
         PlayerRef.Setrace(HumanRace)   

  

         PlayerRef.AddToFaction(RaiderFaction)   ; Raider ally

            
      elseif ProtocolIndex ==  14    ; Dangerous Criminal

         PlayerRef.MoveTo(l_CombatZone)   ; Combat Zone
         PlayerRef.Setrace(HumanRace)   

   
   
         PlayerRef.AddToFaction(CZ_Faction)   ; Combat Zone ally
   
           
         PlayerRef.AddPerk(Pickpocket01)   
         PlayerRef.AddPerk(Pickpocket02)   
         PlayerRef.AddPerk(Pickpocket03)    
         PlayerRef.AddPerk(Locksmith01)   
         PlayerRef.AddPerk(Locksmith02)   
         PlayerRef.AddPerk(Locksmith03)          
 

      elseif ProtocolIndex ==  15    ; Tramp
         PlayerRef.MoveTo(l_Goodneighbour)   
         PlayerRef.Setrace(HumanRace)   

  
         PlayerRef.ModValue(AddictionJet, 10 ) ;
         PlayerRef.AddPerk(Chemist01)
         PlayerRef.AddPerk(Pickpocket01)


      elseif ProtocolIndex ==  16    ; Enclave Remnant Soldier
         PlayerRef.MoveTo(l_GNNGunnerPlazaRailtracks)   ; Gunner plaza train tracks
         PlayerRef.Setrace(HumanRace)   

   
         PlayerRef.ModValue(Strength, 3 ) ; Strength
         PlayerRef.ModValue(Perception, 5 ) ; Perception
         PlayerRef.ModValue(Agility, 4 ) ; Agility

   
         PlayerRef.AddToFaction(OSM_ForgedEnemy)   ;
         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_RaiderEnemy) 

         PlayerRef.AddPerk(Commando01)   
         PlayerRef.AddPerk(Awareness)


      elseif ProtocolIndex ==  17    ; Enclave Remnant Scientist
         PlayerRef.MoveTo(l_GNNGunnerPlazaRailtracks)   ; Gunner plaza train tracks
         PlayerRef.Setrace(HumanRace)   

   

         PlayerRef.ModValue(Intelligence, 7 ) ; Intelligence

   
         PlayerRef.AddToFaction(OSM_ForgedEnemy)   ;
         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_RaiderEnemy) 

         PlayerRef.AddPerk(Science01)   
         PlayerRef.AddPerk(Chemist01)     


      elseif ProtocolIndex ==  18    ; NCR Ranger Veteran
         PlayerRef.MoveTo(l_LonelyChapel)   ; Lonely Chapel
         PlayerRef.Setrace(HumanRace)   

 
         PlayerRef.ModValue(Perception, 9 ) ; Perception
         PlayerRef.ModValue(Endurance, 4 ) ; Endurance
         PlayerRef.ModValue(Agility, 4 ) ; Agility

   
         PlayerRef.AddToFaction(OSM_ForgedEnemy)   ;
         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_RaiderEnemy) 

         PlayerRef.AddPerk(Awareness)


      elseif ProtocolIndex ==  19    ; Shi-Huang-Ti Agent
         PlayerRef.MoveTo(l_DiamondCityExtSecretArea)   
         PlayerRef.Setrace(HumanRace)   
 
   
         PlayerRef.ModValue(Perception, 5 ) ; Perception

   
         PlayerRef.AddToFaction(OSM_ForgedEnemy)   ;
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_RaiderEnemy) 

         PlayerRef.AddPerk(Sneak01)
         PlayerRef.AddPerk(BigLeagues01)

      elseif ProtocolIndex ==  20   ; Safehousescientist (geoligist)
         PlayerRef.MoveTo(l_Institute)   
         PlayerRef.Setrace(HumanRace)   
 
         ;SafehouseTeleporter
         bool InstituteSafeHouseInstalled = Game.IsPluginInstalled("Institute Safe House.esp")
         if InstituteSafeHouseInstalled == true
         Weapon SafehouseTeleporter = Game.GetFormFromFile(0x0176B0, "Institute Safe House.esp") as Weapon
         PlayerRef.Additem(SafehouseTeleporter, 1, false)
         endif

   
         PlayerRef.ModValue(Intelligence, 10 ) ; Intelligence

   
         PlayerRef.AddToFaction(InstituteFaction)   
         PlayerRef.AddToFaction(OSM_RailroadEnemy)
         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy) 

         PlayerRef.AddPerk(Science01)
         PlayerRef.AddPerk(Science02)
         PlayerRef.AddPerk(NuclearPhysicist01)

         PlayerKnowsInstitute.setvalue(1)
         InstituteFirstVisit.setvalue(0)
         PlayerBeenToInstitute.setvalue(1)
         PlayerInstitute_Joined.setvalue(1)


      elseif ProtocolIndex ==  21   ; Bos Knight
         PlayerRef.MoveTo(l_PrydwenHull)
         PlayerRef.ModValue(Strength, 5 ) ; Strength
         PlayerRef.ModValue(Endurance, 4 ) ; Endurance
         PlayerRef.ModValue(Agility, 3 ) ; Agility
         BoSDialoguePrydwen.setstage(10) ;initiate
         BoSDialoguePrydwen.setstage(20) ;Knight
         BoS201.SetStage(4) ;Prydwen arrived, start on Prydwen deck as initiate. Go to briefing, get given Knight rank after Maxson speech
         
         PlayerRef.AddToFaction(BrotherhoodofSteelFaction)   ; BoS Ally


      elseif ProtocolIndex ==  22   ; Bos Paladin
         PlayerRef.MoveTo(l_PrydwenHull)
         PlayerRef.ModValue(Strength, 5 ) ; Strength
         PlayerRef.ModValue(Endurance, 4 ) ; Endurance
         PlayerRef.ModValue(Agility, 3 ) ; Agility

         BoSDialoguePrydwen.setstage(10) ;initiate
         BoSDialoguePrydwen.setstage(20) ;Knight
         BoSDialoguePrydwen.setstage(30) ;Paladin

         BoS302.SetStage(0) ;Speak to Elder Maxson about Danse being suspected Synth

         PlayerRef.AddToFaction(BrotherhoodofSteelFaction)   ; BoS Ally

      elseif ProtocolIndex ==  23   ; Railroad Initiate
         PlayerRef.MoveTo(l_OldNorthChurch)
         PlayerRef.ModValue(Perception, 3 ) 


            RR101.SetStage(300)
            RR101.SetStage(400)

       ;;  RR102.SetStage(0) ;Clear previous quests
      ;;   RR102.SetStage(50) ; talk to Deacon (Quest is Tradecraft btw)

         PlayerRef.AddToFaction(RailroadFaction)   

      elseif ProtocolIndex ==  24   ; Capturing the Castle
         PlayerRef.MoveTo(l_TheCastleRestaurantExt)
         Min02.SetStage(0) ;Clear previous quests + capture the castle
         Min02.SetStage(50)
         PlayerRef.AddToFaction(MinutemenFaction) 

      elseif ProtocolIndex ==  25   ; Cannibal
         PlayerRef.MoveTo(l_Concord) ; Concord EXT

         PlayerRef.addperk(Cannibal01)
         PlayerRef.addperk(Cannibal02)
         PlayerRef.addperk(Cannibal03)


      elseif ProtocolIndex ==  26   ;Courser 
         PlayerRef.MoveTo(l_Institute) ;SynthRetentionBureau
         
         ; Adds this marker to the map
         PlacesOfInterest[1].AddtoMap() ;Institute
         PlacesOfInterest[1].Enable()

         PlayerRef.AddToFaction(InstituteFaction)

         MQ207.SetStage(0)
         MQ207.SetStage(1)
         MQ207.SetStage(150)
         PlayerInstitute_Joined.SetValue(1)
         MQ207.SetStage(200)
               
               



      elseif ProtocolIndex ==  27   ;Ins AdvSci Scientist

         PlayerRef.MoveTo(l_Institute) ;AdvSystems
         
         ; Adds this marker to the map
         PlacesOfInterest[1].AddtoMap() ;Institute
         PlacesOfInterest[1].Enable()

         PlayerRef.AddToFaction(InstituteFaction)

         MQ207.SetStage(0)
         MQ207.SetStage(1)
         MQ207.SetStage(150)
         PlayerInstitute_Joined.SetValue(1)
         MQ207.SetStage(200)
         
         


      elseif ProtocolIndex ==  28   ;Survivalist
            PlayerRef.MoveTo(l_DiamondCityExtSecretArea) ;Diamond City Ext secret location/hideout den thing on roof
            PlayerRef.addperk(LoneWanderer01)
            PlayerRef.addperk(Toughness01)
            PlayerRef.addperk(RadResistant01)
            PlayerRef.addperk(Scrounger01)


      elseif ProtocolIndex ==  29   ;Tinkerer

      elseif ProtocolIndex ==  30   ;Escaped Gen2Synth   
      elseif ProtocolIndex ==  31   ;Vault81 Villain
      elseif ProtocolIndex ==  32   ;NomadTrader

      elseif ProtocolIndex ==  33   ;Purger
         PlayerRef.MoveTo(l_SaugausIronworks01) ;forged base
         PlayerRef.addperk(BloodyMessPerk01)
         PlayerRef.addperk(HeavyGunner01)
         PlayerRef.AddToFaction(TheForgedFaction)
         PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)
         PlayerRef.AddToFaction(OSM_GunnerEnemy)
         PlayerRef.AddToFaction(OSM_MinutemenEnemy)


      elseif ProtocolIndex ==  34   ;Getting Rusty
         PlayerRef.MoveTo(l_FortHagenSatelliteArray) ;FortHagenSateliteArray
         PlayerRef.AddPerk(RoboticsExpert01)
         PlayerRef.AddPerk(RoboticsExpert02)
         PlayerRef.AddPerk(Science01)
         PlayerRef.AddPerk(Science02)






      elseif ProtocolIndex ==  35   ;Desert Ranger 
      elseif ProtocolIndex ==  36
      elseif ProtocolIndex ==  37
      elseif ProtocolIndex ==  38
      elseif ProtocolIndex ==  39
      elseif ProtocolIndex ==  40












      EndIf 
      ProtocolLoaded()
      EndFunction

Function LoadCustomProtocol()
      ChosenLocation()
      ChosenEquipment()
      ChosenRace()
      ChosenFaction()
      ProtocolLoaded()
      EndFunction

Function LoadDLCProtocol()

   EndFunction

Function LoadModProtocol()

   EndFunction 



Function ProtocolLoaded() ; Make the player got to 3rd person camera mode

            Game.ForceThirdPerson() 
;;;            PlayerRef.Additem(OSM_AlternateStart_Holotape_Settings, 1 , false) ; Post Glitch Note
    
            OSM_PlayingVanilla.SetValue(0)



            ;Force open diamond city
            ;clean up diamond city
            DialogueDiamondCityEntrance.SetStage(1000) ;only activates if in proximity. Trigger added outside DC to also call this setstage 

            ;Allow player to talk to Piper to recruit, since we skipped that in DialogueDiamondCityEntrance
            FFDiamondCity07.SetStage(5)
            ;allow time to advance briefly so the clock on the pipboy gets set
            TimeScale.SetValueInt(20)
            ; CharGen over!  Back to standard HUD mode
            Game.SetCharGenHUDMode(0)
            ;allow saving
            Game.SetInCharGen(False, False, False)



            ;Turn on Diamond City Radio
            RadioDiamondCity.Start()
   
            ;Turn on Classical Radio 
            RadioInstituteQuest.Start()
   
            ;Also turn on BoSM01 and its Distress Signal radio stations at this point.
;;;STILL REQUIRED?         BoSM01.SetStage(10)

            MQ102.setstage(9)
            MQ102.setstage(10)




            BoSEnable.setstage(10) ;boston airport BoS
            BoSEnable.setstage(15) ;Prydwen



            Debug.Notification("Alternate Start Finished")
            Debug.MessageBox("Alternate Start Finished")


            ; UnMute all sounds(which should still be in synch with their animations.)
            _AudioCategoryMaster.UnMute()
            ; Restart sounds.
            _AudioCategoryMaster.UnPause()
   
   
            ; Adds this marker to the map
            PlacesOfInterest[3].AddtoMap() ;Diamond City
            PlacesOfInterest[3].Enable()


            Game.RequestAutoSave() 
            Utility.Wait(1.0)
                
            EndFunction


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;**********************Custom Protocols***********************;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Function ChosenLocation()
   If ChosenLocation == 0
   PlayerRef.MoveTo(StartingLocation[  0  ]) 
               
   ElseIf ChosenLocation   == 1
   PlayerRef.MoveTo(StartingLocation[  1  ])
            
   ElseIf ChosenLocation   == 2
   PlayerRef.MoveTo(StartingLocation[  2  ])
            
   ElseIf ChosenLocation   == 3
   PlayerRef.MoveTo(StartingLocation[  3  ])
         
   ElseIf ChosenLocation   == 4
   PlayerRef.MoveTo(StartingLocation[  4  ])
         
   ElseIf ChosenLocation   == 5
   PlayerRef.MoveTo(StartingLocation[  5  ])
         
   ElseIf ChosenLocation   == 6
   PlayerRef.MoveTo(StartingLocation[  6  ])
         
   ElseIf ChosenLocation   == 7
   PlayerRef.MoveTo(StartingLocation[  7  ])
         
   ElseIf ChosenLocation   == 8
   PlayerRef.MoveTo(StartingLocation[  8  ])
         
   ElseIf ChosenLocation   == 9
   PlayerRef.MoveTo(StartingLocation[  9  ])
         
   ElseIf ChosenLocation   == 10
   PlayerRef.MoveTo(StartingLocation[  10 ])
         
   ElseIf ChosenLocation   == 11
   PlayerRef.MoveTo(StartingLocation[  11 ])
         
   ElseIf ChosenLocation   == 12
   PlayerRef.MoveTo(StartingLocation[  12 ])
         
   ElseIf ChosenLocation   == 13
   PlayerRef.MoveTo(StartingLocation[  13 ])
         
   ElseIf ChosenLocation   == 14
   PlayerRef.MoveTo(StartingLocation[  14 ])

   EndIf    
   EndFunction
Function ChosenEquipment()
         PlayerRef.Additem(Caps001, 250 , true)         
         PlayerRef.Additem(BobbyPin, 2 , true)



         If ChosenEquipment == 0
         PlayerRef.Additem(Stimpak, 3 , true)
         PlayerRef.Additem(RadAway, 3 , true)
         PlayerRef.Additem(Pistol10mm, 1 , true) 
         PlayerRef.EquipItem(Pistol10mm,  false, true)
         PlayerRef.Additem(Ammo10mm, 50 , true) 
         PlayerRef.Additem(Armor_GunnerFlannelShirtandJeans, 1 , true) ; Gunner Shirt Jeans
         PlayerRef.EquipItem(Armor_GunnerFlannelShirtandJeans,  false, true)  ; 
         PlayerRef.Additem(Armor_Combat_Torso, 1 , true) ; Combat
         PlayerRef.EquipItem(Armor_Combat_Torso,  false, true)
         PlayerRef.AddPerk(LoneWanderer01)     
         ElseIf ChosenEquipment == 1
         PlayerRef.Additem(Stimpak, 3 , true)
         PlayerRef.Additem(RadX, 3 , true)
         PlayerRef.Additem(RadAway, 3 , true)
         PlayerRef.Additem(LaserGun, 1 , true) 
         PlayerRef.EquipItem(LaserGun,  false, true)
         PlayerRef.Additem(AmmoFusionCell, 50 , true)
         PlayerRef.Additem(ClothesVaultTecScientist, 1 , true) ; Wastelander Light
         PlayerRef.EquipItem(ClothesVaultTecScientist,  false, true)  ;
         PlayerRef.ModValue(Intelligence, 7 ) ; Intelligence  
         PlayerRef.AddPerk(Science01) 
         PlayerRef.AddPerk(ChemResistant01)    

         ElseIf ChosenEquipment == 2
         PlayerRef.Additem(Jet, 3 , true)
         PlayerRef.Additem(Psycho, 3 , true)
         PlayerRef.Additem(Mentats, 3 , true)
         PlayerRef.Additem(Switchblade, 1 , true) 
         PlayerRef.EquipItem(Switchblade,  false, true)
         PlayerRef.Additem(ClothesResident6, 1 , true) ; Wastelander Light
         PlayerRef.EquipItem(ClothesResident6,  false, true)  ;  
         PlayerRef.ModValue(AddictionJet, 10 ) ;
         PlayerRef.AddPerk(Chemist01)

         ElseIf ChosenEquipment == 3
         PlayerRef.Additem(Stimpak, 2 , true)
         PlayerRef.Additem(WaterPurified, 9 , true)
         PlayerRef.Additem(RadAway, 2 , true)  
         PlayerRef.Additem(DoubleBarrelShotgun, 1 , true) 
         PlayerRef.EquipItem(DoubleBarrelShotgun,  false, true)
         PlayerRef.Additem(AmmoShotgunShell, 50 , true) 
         PlayerRef.Additem(ClothesResident6, 1 , true) ; Wastelander Light
         PlayerRef.Additem(ClothesResident6, 1 , true) ; Wastelander Light
         PlayerRef.EquipItem(ClothesResident6,  false, true)  ; 
         PlayerRef.Additem(Armor_Leather_Torso, 1 , true) ; Leather
         PlayerRef.EquipItem(Armor_Leather_Torso,  false, true)  ; 
         PlayerRef.Additem(ClothesGrayKnitCap, 1 , true) ; Gray knit cap
         PlayerRef.EquipItem(ClothesGrayKnitCap,  false, true)  ; 
         PlayerRef.AddPerk(CapCollector01)
         PlayerRef.AddPerk(CapCollector02)
         PlayerRef.ModValue(Charisma, 7)

         ElseIf ChosenEquipment == 4
         PlayerRef.Additem(WaterPurified, 3 , true)
         PlayerRef.Additem(SteakBloatfly, 1 , true)
         PlayerRef.Additem(SteakBloodbug, 1 , true)     
         PlayerRef.Additem(SteakDeathclaw, 1 , true)
         PlayerRef.Additem(SteakRadroach, 1 , true)
         PlayerRef.Additem(SteakRadStag, 1 , true)      
         PlayerRef.Additem(SteakMolerat, 1 , true)
         PlayerRef.Additem(IguanaStick, 3 , true)
         PlayerRef.Additem(SteakRadscorpion, 1 , true)
         PlayerRef.Additem(HuntingRifle, 1 , true) 
         PlayerRef.EquipItem(HuntingRifle,  false, true)
         PlayerRef.Additem(Ammo38Caliber, 50 , true)
         PlayerRef.Additem(ClothesResident6, 1 , true) ; Wastelander Light
         PlayerRef.EquipItem(ClothesResident6,  false, true)  ; 
         PlayerRef.Additem(Armor_Leather_Torso, 1 , true) ; Leather
         PlayerRef.EquipItem(Armor_Leather_Torso,  false, true)  ; 
         PlayerRef.Additem(ClothesGrayKnitCap, 1 , true) ; Gray knit cap
         PlayerRef.EquipItem(ClothesGrayKnitCap,  false, true)  ;
         PlayerRef.ModValue(Perception, 7 ) ; Perception
         PlayerRef.AddPerk(Awareness)   

                     
         ElseIf ChosenEquipment == 5
         PlayerRef.Additem(Stimpak, 2 , true)
         PlayerRef.Additem(Vodka, 3 , true)
         PlayerRef.Additem(Bourbon, 3 , true)        
         PlayerRef.Additem(Rum, 2 , true)
         PlayerRef.ModValue(AddictionAlcohol, 10 ) ; 

         EndIf
         EndFunction
Function ChosenRace()
   If ChosenRace == 0
   PlayerRef.Setrace(HumanRace) 
   ElseIf ChosenRace == 1
   PlayerRef.Setrace(GhoulRace) 
   ElseIf ChosenRace == 2
   PlayerRef.Setrace(SynthGen1Race) 
   ElseIf ChosenRace == 3
   PlayerRef.Setrace(SynthGen2Race)
   ElseIf ChosenRace == 4
   PlayerRef.Setrace(HumanRace)                            
   EndIf    
   EndFunction        
Function ChosenFaction()
   If ChosenFaction == 0
   ;do nothing as newcommer = neutral/vanilla
   ElseIf ChosenFaction == 1
   PlayerRef.AddToFaction(MinutemenFaction)
   ElseIf ChosenFaction == 2
   PlayerRef.AddToFaction(RailroadFaction)
   ElseIf ChosenFaction == 3
   PlayerRef.AddToFaction(InstituteFaction)
   ElseIf ChosenFaction == 4
   PlayerRef.AddToFaction(RaiderFaction)
   ElseIf ChosenFaction == 5
   PlayerRef.AddToFaction(GunnerFaction) 
   ElseIf ChosenFaction == 6
   PlayerRef.AddToFaction(BrotherhoodofSteelFaction)
   ElseIf ChosenFaction == 7
   PlayerRef.AddToFaction(TheForgedFaction)
   ElseIf ChosenFaction == 8
   PlayerRef.AddToFaction(GoodneighborGenericNPCFaction)
   ElseIf ChosenFaction == 9
   PlayerRef.AddToFaction(REScavengerFaction)
   ElseIf ChosenFaction == 10
   PlayerRef.AddToFaction(ChildrenOfAtomFaction)
   ElseIf ChosenFaction == 11
   PlayerRef.AddToFaction(DN054AtomCatFaction)
   
   ElseIf ChosenFaction == 12
   PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy)
   ElseIf ChosenFaction == 13
   PlayerRef.AddToFaction(OSM_ForgedEnemy)
   ElseIf ChosenFaction == 14
   PlayerRef.AddToFaction(OSM_GunnerEnemy)
   ElseIf ChosenFaction == 15
   PlayerRef.AddToFaction(OSM_InstituteEnemy)
   ElseIf ChosenFaction == 16
   PlayerRef.AddToFaction(OSM_MinutemenEnemy) 
   ElseIf ChosenFaction == 17
   PlayerRef.AddToFaction(OSM_RaiderEnemy)
   ElseIf ChosenFaction == 18
   PlayerRef.AddToFaction(OSM_RailroadEnemy) 

   ElseIf ChosenFaction == 19
   PlayerRef.AddToFaction(OSM_BrotherhoodofSteelEnemy) 
   PlayerRef.AddToFaction(OSM_InstituteEnemy)
   PlayerRef.AddToFaction(OSM_MinutemenEnemy)
   PlayerRef.AddToFaction(OSM_RailroadEnemy)
   ElseIf ChosenFaction == 20
   PlayerRef.AddToFaction(RaiderFaction)
   PlayerRef.AddToFaction(GunnerFaction)
   PlayerRef.AddToFaction(TheForgedFaction)
 

   EndIf    
   EndFunction  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;**********************Custom Protocols***********************;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;**********************Variables***********************;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   
         Group Choices
         Int Property ProtocolIndex = 0 Auto
         Int Property ChosenLocation Auto
         Int Property ChosenEquipment Auto
         Int Property ChosenRace Auto
         Int Property ChosenFaction Auto
         endGroup





         Group Locations
         ObjectReference[] Property StartingLocation Auto Const
         ObjectReference[] Property PlacesOfInterest Auto Const
         ObjectReference Property l_Goodneighbour Auto Const
         ObjectReference Property l_GlowingSeaCrater Auto Const
         ObjectReference Property l_Vault111Ext Auto Const
         ObjectReference Property l_DiamondCityMarket Auto Const
         ObjectReference Property l_WarwickHomestead Auto Const
         ObjectReference Property l_CambridgePD Auto Const
         ObjectReference Property l_Shipwreck Auto Const
         ObjectReference Property l_FiddlersGreen Auto Const
         ObjectReference Property l_SpectacleIsland Auto Const
         ObjectReference Property l_JamaicaPlainExt Auto Const
         ObjectReference Property l_InstituteMIT Auto Const
         ObjectReference Property l_CorvegaAssemblyPlant Auto Const
         ObjectReference Property l_LibertaliaNorth Auto Const
         ObjectReference Property l_DunwichBorers Auto Const
         ObjectReference Property l_GNNGunnerPlazaRailtracks Auto Const
         ObjectReference Property l_Institute Auto Const
         ObjectReference Property l_PrydwenHull Auto Const
         ObjectReference Property l_VirgilsCave Auto Const
         ObjectReference Property l_TheCastle Auto Const
         ObjectReference Property l_TheCastleRestaurantExt Auto Const
         ObjectReference Property l_RailroadHQ Auto Const
         ObjectReference Property l_ValentinesDetectiveAgency Auto Const
         ObjectReference Property l_OldNorthChurch Auto Const
         ObjectReference Property l_Concord Auto Const
         ObjectReference Property l_FortHagenSatelliteArray Auto Const
         ObjectReference Property l_DiamondCityExtSecretArea Auto Const
         ObjectReference Property l_SaugausIronworks01 Auto Const
         ObjectReference Property l_CombatZone Auto Const
         ObjectReference Property l_LonelyChapel Auto Const

         endGroup

         Group OtherVariables
         Actor Property PlayerRef Auto
         Quest[] Property QuestRefs Auto Const
         ObjectReference[] Property Vault111Refs Auto Const
         Book Property OSM_Note  Auto Const
         Book[] Property ProtocolNote  Auto Const
         Holotape Property OSM_AlternateStart_Holotape_Settings Auto Const
         GlobalVariable Property TimeScale Auto Const
         GlobalVariable Property OSM_PlayingVanilla Auto
         Int Property OSM_UsingCustomProtocol Auto
         GlobalVariable Property RailroadClothingArmorModAvailable Auto
         Key Property OSM_AlternateCellKey Auto Const
         GlobalVariable Property OSM_CustomProtocolIndex Auto

         GlobalVariable Property PlayerKnowsInstitute Auto
         GlobalVariable Property InstituteFirstVisit Auto
         GlobalVariable Property PlayerBeenToInstitute Auto
         GlobalVariable Property PlayerInstitute_Joined Auto
         GlobalVariable Property BoSPrydwenArrived Auto
         
         
         
         endGroup
         
         Group Quests
         Quest Property MQ101 Auto Const
         Quest Property MQ102 Auto Const
         Quest Property MQ103 Auto Const
         Quest Property MQ104 Auto Const
         Quest Property MQ105 Auto Const
         Quest Property MQ106 Auto Const
         Quest Property MQ201 Auto Const
         Quest Property MQ202 Auto Const
         Quest Property MQ204 Auto Const

         
         Quest Property MQ205 Auto Const
         Quest Property MQ206 Auto Const
         Quest Property MQ207 Auto Const


         Quest Property BoSDialoguePrydwen Auto Const ;use this to set player BoS rank
         Quest Property BoSEnable Auto Const ;use this to set whether Prydwen/Liberty etc are active

         Quest Property BoS000 Auto Const
         Quest Property BoS100 Auto Const
         Quest Property BoS101 Auto Const
         Quest Property BoS200 Auto Const
         Quest Property BoS201 Auto Const
         Quest Property BoS201B Auto Const
         Quest Property BoS202 Auto Const
         Quest Property BoS203 Auto Const
         Quest Property BoS204 Auto Const
         Quest Property BoS301 Auto Const
         Quest Property BoS302 Auto Const
         Quest Property BoS302B Auto Const
         Quest Property BoS303 Auto Const
         Quest Property BoS304 Auto Const
         Quest Property MQ302BoS Auto Const
         Quest Property BoS305 Auto Const

         Quest Property RR101 Auto Const
         Quest Property RR102 Auto Const
         Quest Property RR201 Auto Const
         Quest Property RR302 Auto Const
         Quest Property RR303 Auto Const
         Quest Property MQ302RR Auto Const


         Quest Property Inst301 Auto Const
         Quest Property Inst302 Auto Const
         Quest Property Inst303 Auto Const
         Quest Property InstMassFusion Auto Const
         Quest Property MinVsInst Auto Const
         Quest Property Inst305 Auto Const
         Quest Property Inst306 Auto Const
         Quest Property Inst307 Auto Const
         Quest Property Inst308 Auto Const
         Quest Property InstKickOut Auto Const

         Quest Property Min00 Auto Const
         Quest Property MinRecruit00 Auto Const ;RECRUIT PRESTON
         Quest Property Min02 Auto Const
         Quest Property Min03 Auto Const
         Quest Property Min207 Auto Const
         Quest Property Min301 Auto Const
         Quest Property MinDefendCastle Auto Const
         Quest Property MQ302Min Auto Const
         Quest Property MinDestBoS Auto Const


         Quest Property RadioDiamondCity Auto Const
         Quest Property RadioInstituteQuest Auto Const
         Quest Property DialogueDiamondCityEntrance Auto Const
         Quest Property FFDiamondCity07 Auto Const
         Quest Property MS07 Auto Const
         Quest Property MS07Intro Auto Const



         


         endGroup

         Group Factions

         Faction Property OSM_BrotherhoodofSteelEnemy Auto Const
         Faction Property OSM_ForgedEnemy Auto Const
         Faction Property OSM_GunnerEnemy Auto Const
         Faction Property OSM_InstituteEnemy Auto Const
         Faction Property OSM_MinutemenEnemy Auto Const
         Faction Property OSM_RaiderEnemy Auto Const
         Faction Property OSM_RailroadEnemy Auto Const

         Faction Property BrotherhoodofSteelFaction Auto Const
         Faction Property TheForgedFaction Auto Const
         Faction Property GunnerFaction Auto Const
         Faction Property InstituteFaction Auto Const
         Faction Property MinutemenFaction Auto Const
         Faction Property RaiderFaction Auto Const
         Faction Property RailroadFaction Auto Const
         Faction Property CZ_Faction Auto Const

         Faction Property GoodneighborGenericNPCFaction Auto Const
         Faction Property REScavengerFaction Auto Const
         Faction Property ChildrenOfAtomFaction Auto Const
         Faction Property DN054AtomCatFaction Auto Const
         endGroup

         Group Races
         Race Property GhoulRace Auto Const
         Race Property HumanRace Auto Const
         Race Property SynthGen1Race Auto Const
         Race Property SynthGen2Race Auto Const
         endGroup



         Group Items
         Potion Property Stimpak Auto Const
         Potion Property RadX Auto Const
         Potion Property RadAway Auto Const
         Potion Property Psycho Auto Const
         Potion Property MedX Auto Const
         Potion Property Jet Auto Const
         Potion Property Mentats Auto Const
         Potion Property Addictol Auto Const
         Potion Property WaterDirty Auto Const
         Potion Property WaterPurified Auto Const
         Potion Property SteakBloatfly Auto Const
         Potion Property SteakBloodbug Auto Const
         Potion Property SteakDeathclaw Auto Const
         Potion Property SteakRadroach Auto Const
         Potion Property SteakRadStag Auto Const
         Potion Property SteakMolerat Auto Const
         Potion Property SteakRadscorpion Auto Const
         Potion Property SteakBrahmin Auto Const
         Potion Property IguanaStick Auto Const
         Potion Property Vodka Auto Const
         Potion Property Bourbon Auto Const
         Potion Property Rum Auto Const

         MiscObject Property Caps001 Auto Const
         MiscObject Property BobbyPin Auto Const
         endGroup



         Group Weapons

         Weapon Property Pistol10mm Auto Const
         Weapon Property Pistol44 Auto Const
         Weapon Property AlienBlaster Auto Const
         Weapon Property AssaultRifle Auto Const
         Weapon Property Cryolator Auto Const
         Weapon Property CombatRifle Auto Const
         Weapon Property CombatShotgun Auto Const
         Weapon Property Deliverer Auto Const
         Weapon Property DoubleBarrelShotgun Auto Const
         Weapon Property Fatman Auto Const
         Weapon Property Flamer Auto Const
         Weapon Property FlareGun Auto Const
         Weapon Property GammaGun Auto Const
         Weapon Property GatlingLaser Auto Const
         Weapon Property GaussRifle Auto Const
         Weapon Property HuntingRifle Auto Const
         Weapon Property InstituteLaserGun Auto Const
         Weapon Property JunkJet Auto Const
         Weapon Property LaserGun Auto Const
         Weapon Property LaserMusket Auto Const
         Weapon Property Minigun Auto Const
         Weapon Property MissileLauncher Auto Const
         Weapon Property PipeBoltAction Auto Const
         Weapon Property PipeGun Auto Const
         Weapon Property PipeRevolver Auto Const
         Weapon Property PipeSyringer Auto Const
         Weapon Property PlasmaGun Auto Const
         Weapon Property RailwayRifle Auto Const
         Weapon Property SubmachineGun Auto Const
         ObjectMod Property Sniper50CalReceiverMod Auto Const


         Weapon Property BaseballBat Auto Const
         Weapon Property BoxingGlove Auto Const
         Weapon Property ChineseOfficerSword Auto Const
         Weapon Property Baton Auto Const
         Weapon Property Board Auto Const
         Weapon Property LeadPipe Auto Const
         Weapon Property Machete Auto Const
         Weapon Property Knife Auto Const
         Weapon Property Knuckles Auto Const
         Weapon Property PipeWrench Auto Const
         Weapon Property PoolCue Auto Const
         Weapon Property PowerFist Auto Const
         Weapon Property RevolutionarySword Auto Const
         Weapon Property Ripper Auto Const
         Weapon Property RollingPin Auto Const
         Weapon Property Shishkebab Auto Const
         Weapon Property Sledgehammer Auto Const
         Weapon Property SuperSledge Auto Const
         Weapon Property Switchblade Auto Const
         Weapon Property TireIron Auto Const
         Weapon Property WalkingCane Auto Const


         Weapon Property InstituteBeaconGrenade Auto Const
         Weapon Property fragMine Auto Const
         Weapon Property PulseMine Auto Const
         Weapon Property PlasmaMine Auto Const
         Weapon Property NukeMine Auto Const
         Weapon Property CryoMine Auto Const
         Weapon Property BottlecapMine Auto Const


         Weapon Property SynthTeleportGrenade Auto Const
         Weapon Property fragGrenade Auto Const
         Weapon Property MolotovCocktail Auto Const
         Weapon Property PulseGrenade Auto Const
         Weapon Property CryoGrenade Auto Const
         Weapon Property PlasmaGrenade Auto Const
         Weapon Property nukaGrenade Auto Const
         Weapon Property BaseballGrenade Auto Const
         endGroup



         Group Ammo
         Ammo Property Ammo10mm Auto Const
         Ammo Property Ammo2mmEC Auto Const
         Ammo Property Ammo308Caliber Auto Const
         Ammo Property Ammo38Caliber Auto Const
         Ammo Property Ammo44 Auto Const
         Ammo Property Ammo45Caliber Auto Const
         Ammo Property Ammo50Caliber Auto Const
         Ammo Property Ammo556 Auto Const
         Ammo Property Ammo5mm Auto Const
         Ammo Property AmmoAlienBlaster Auto Const
         Ammo Property AmmoCryoCell Auto Const
         Ammo Property AmmoFatManMiniNuke Auto Const
         Ammo Property AmmoFlamerFuel Auto Const
         Ammo Property AmmoFlareGun Auto Const
         Ammo Property AmmoFusionCell Auto Const
         Ammo Property AmmoFusionCore Auto Const
         Ammo Property AmmoGammaCell Auto Const
         Ammo Property AmmoMissile Auto Const
         Ammo Property AmmoPlasmaCartridge Auto Const
         Ammo Property AmmoRRSpike Auto Const
         Ammo Property AmmoShotgunShell Auto Const
         endGroup


         Group Armor
         Armor Property Armor_Combat_ArmLeft Auto Const
         Armor Property Armor_Combat_ArmRight Auto Const
         Armor Property Armor_Combat_Helmet Auto Const
         Armor Property Armor_Combat_LegLeft Auto Const
         Armor Property Armor_Combat_LegRight Auto Const
         Armor Property Armor_Combat_Torso Auto Const
         Armor Property Armor_Leather_ArmLeft Auto Const
         Armor Property Armor_Leather_ArmRight Auto Const
         Armor Property Armor_Leather_LegLeft Auto Const
         Armor Property Armor_Leather_LegRight Auto Const
         Armor Property Armor_Leather_Torso Auto Const
         Armor Property Armor_Metal_ArmLeft Auto Const
         Armor Property Armor_Metal_ArmRight Auto Const
         Armor Property Armor_Metal_Helmet Auto Const
         Armor Property Armor_Metal_LegLeft Auto Const
         Armor Property Armor_Metal_LegRight Auto Const
         Armor Property Armor_Metal_Torso Auto Const
         Armor Property Armor_RaiderMod_ArmLeft Auto Const
         Armor Property Armor_RaiderMod_ArmRight Auto Const
         Armor Property Armor_RaiderMod_LegLeft Auto Const
         Armor Property Armor_RaiderMod_LegRight Auto Const
         Armor Property Armor_RaiderMod_Torso Auto Const
         Armor Property Armor_Synth_ArmLeft Auto Const
         Armor Property Armor_Synth_ArmRight Auto Const
         Armor Property Armor_Synth_HelmetClosed Auto Const
         Armor Property Armor_Synth_LegLeft Auto Const
         Armor Property Armor_Synth_LegRight Auto Const
         Armor Property Armor_Synth_Torso Auto Const






         Armor Property Pipboy Auto Const
         Armor Property Clothes_InstituteUniform Auto Const
         Armor Property ClothesSuitDirty_Stripes Auto Const
         Armor Property ClothesChildOfAtom Auto Const
         Armor Property Armor_Vault111_Jumpsuit Auto Const ;Vault111Clean_Underwear
         Armor Property ClothesGenericJumpsuit Auto Const ;Mechanic Jumpsuit
         Armor Property Armor_BoS_Soldier_Jumpsuit Auto Const ;BoSSoldierUnderarmor
         Armor Property Armor_GunnerLeathers Auto Const ;GunnerRaiderMod_Underarmor
         Armor Property Armor_GunnerFlannelShirtandJeans Auto Const ;Armor_GunnerRustic_Underarmor
         Armor Property ClothesResident6 Auto Const ;Hooded Rags
         Armor Property MS02_ClothesSubmarineCrew Auto Const ;SubUniform
         Armor Property MS02_ClothesSubmarineCrewHat Auto Const ;Sub Hat
         Armor Property Armor_Raider_Gaskmask Auto Const
         Armor Property Armor_Gaskmask Auto Const;GaskMaskWithGoggles
         Armor Property Armor_HazmatSuit Auto Const
         Armor Property ClothesWrappedCap Auto Const ;Clothes_raider_hood
         Armor Property ClothesMilitaryFatigues Auto Const
         Armor Property ClothesTuxedo Auto Const
         Armor Property ClothesSlinkyDress Auto Const 
         Armor Property ClothesSequinDress Auto Const
         Armor Property ClothesGrayKnitCap Auto Const
         Armor Property ClothesBaseballHat Auto Const
         Armor Property ClothesResident3Hat Auto Const
         Armor Property ClothesFancyGlasses Auto Const
         Armor Property ClothesBlackRimGlasses Auto Const
         Armor Property ClothesEyeGlasses Auto Const
         Armor Property ClothesSunGlasses Auto Const
         Armor Property Clothes_Raider_Goggles Auto Const
         Armor Property ClothesVaultTecScientist Auto Const
         Armor Property CharGenPlayerClothes Auto Const
         Armor Property Armor_WeddingRing Auto Const
         endGroup





         Group PowerArmor
         Furniture Property FramePowerArmor Auto Const
         Furniture Property LeveledPowerArmor Auto Const
         Furniture Property RandomizedNoCorePowerArmor Auto Const
         Furniture Property RaiderLeveledPowerArmor Auto Const
         Furniture Property T45PowerArmor Auto Const
         Furniture Property T51PowerArmor Auto Const
         Furniture Property T60PowerArmor Auto Const
         Furniture Property X01PowerArmor Auto Const
         endGroup

         Group ActorValues
         ActorValue Property Health Auto Const
         ActorValue Property ActionPoints Auto Const
         ActorValue Property HealRate Auto Const
         ActorValue Property ActionPointsRate Auto Const
         ActorValue Property CarryWeight Auto Const
         ActorValue Property CritChance Auto Const
         ActorValue Property MeleeDamage Auto Const
         ActorValue Property UnarmedDamage Auto Const
         ActorValue Property DamageResist Auto Const
         ActorValue Property PoisonResist Auto Const
         ActorValue Property FireResist Auto Const
         ActorValue Property ElectricResist Auto Const
         ActorValue Property FrostResist Auto Const
         ActorValue Property MagicResist Auto Const
         ActorValue Property RadResistIngestion Auto Const
         ActorValue Property RadResistExposure Auto Const
         ActorValue Property EnergyResist Auto Const
         ActorValue Property AddictionAlcohol Auto Const
         ActorValue Property AddictionJet Auto Const
         ActorValue Property AddictionPsycho Auto Const
         ActorValue Property Strength Auto Const
         ActorValue Property Perception Auto Const
         ActorValue Property Endurance Auto Const
         ActorValue Property Charisma Auto Const
         ActorValue Property Intelligence Auto Const
         ActorValue Property Agility Auto Const
         ActorValue Property Luck Auto Const

         endGroup

         Group Perks
         Perk Property IronFist01 Auto Const
         Perk Property IronFist02 Auto Const
         Perk Property IronFist03 Auto Const
         Perk Property IronFist04 Auto Const
         Perk Property IronFist05 Auto Const
         Perk Property BigLeagues01 Auto Const
         Perk Property BigLeagues02 Auto Const
         Perk Property BigLeagues03 Auto Const
         Perk Property BigLeagues04 Auto Const
         Perk Property BigLeagues05 Auto Const
         Perk Property Armorer01 Auto Const
         Perk Property Armorer02 Auto Const
         Perk Property Armorer03 Auto Const
         Perk Property Armorer04 Auto Const
         Perk Property Blacksmith01 Auto Const
         Perk Property Blacksmith02 Auto Const
         Perk Property Blacksmith03 Auto Const
         Perk Property HeavyGunner01 Auto Const
         Perk Property HeavyGunner02 Auto Const
         Perk Property HeavyGunner03 Auto Const
         Perk Property HeavyGunner04 Auto Const
         Perk Property HeavyGunner05 Auto Const
         Perk Property StrongBack01 Auto Const
         Perk Property StrongBack02 Auto Const
         Perk Property StrongBack03 Auto Const
         Perk Property StrongBack04 Auto Const
         Perk Property SteadyAim01 Auto Const
         Perk Property SteadyAim02 Auto Const
         Perk Property Basher01 Auto Const
         Perk Property Basher02 Auto Const
         Perk Property Basher03 Auto Const
         Perk Property Rooted01 Auto Const
         Perk Property Rooted02 Auto Const
         Perk Property Rooted03 Auto Const
         Perk Property PainTrain01 Auto Const
         Perk Property PainTrain02 Auto Const
         Perk Property PainTrain03 Auto Const
         Perk Property Pickpocket01 Auto Const
         Perk Property Pickpocket02 Auto Const
         Perk Property Pickpocket03 Auto Const
         Perk Property Pickpocket04 Auto Const
         Perk Property Rifleman01 Auto Const
         Perk Property Rifleman02 Auto Const
         Perk Property Rifleman03 Auto Const
         Perk Property Rifleman04 Auto Const
         Perk Property Rifleman05 Auto Const
         Perk Property Awareness Auto Const
         Perk Property Locksmith01 Auto Const
         Perk Property Locksmith02 Auto Const
         Perk Property Locksmith03 Auto Const
         Perk Property Locksmith04 Auto Const
         Perk Property DemolitionExpert01 Auto Const
         Perk Property DemolitionExpert02 Auto Const
         Perk Property DemolitionExpert03 Auto Const
         Perk Property DemolitionExpert04 Auto Const
         Perk Property NightPerson01 Auto Const
         Perk Property NightPerson02 Auto Const
         Perk Property Refractor01 Auto Const
         Perk Property Refractor02 Auto Const
         Perk Property Refractor03 Auto Const
         Perk Property Refractor04 Auto Const
         Perk Property Refractor05 Auto Const
         Perk Property Sniper01 Auto Const
         Perk Property Sniper02 Auto Const
         Perk Property Sniper03 Auto Const
         Perk Property Penetrator01 Auto Const
         Perk Property Penetrator02 Auto Const
         Perk Property ConcentratedFire01 Auto Const
         Perk Property ConcentratedFire02 Auto Const
         Perk Property ConcentratedFire03 Auto Const
         Perk Property Toughness01 Auto Const
         Perk Property Toughness02 Auto Const
         Perk Property Toughness03 Auto Const
         Perk Property Toughness04 Auto Const
         Perk Property Toughness05 Auto Const
         Perk Property LeadBelly01 Auto Const
         Perk Property LeadBelly02 Auto Const
         Perk Property LeadBelly03 Auto Const
         Perk Property LifeGiver01 Auto Const
         Perk Property LifeGiver02 Auto Const
         Perk Property LifeGiver03 Auto Const
         Perk Property ChemResistant01 Auto Const
         Perk Property ChemResistant02 Auto Const
         Perk Property Aquaboy01 Auto Const
         Perk Property Aquaboy02 Auto Const
         Perk Property Aquagirl01 Auto Const
         Perk Property Aquagirl02 Auto Const
         Perk Property RadResistant01 Auto Const
         Perk Property RadResistant02 Auto Const
         Perk Property RadResistant03 Auto Const
         Perk Property AdamantiumSkeleton01 Auto Const
         Perk Property AdamantiumSkeleton02 Auto Const
         Perk Property AdamantiumSkeleton03 Auto Const
         Perk Property Cannibal01 Auto Const
         Perk Property Cannibal02 Auto Const
         Perk Property Cannibal03 Auto Const
         Perk Property Ghoulish01 Auto Const
         Perk Property Ghoulish02 Auto Const
         Perk Property Ghoulish03 Auto Const
         Perk Property SolarPowered01 Auto Const
         Perk Property SolarPowered02 Auto Const
         Perk Property SolarPowered03 Auto Const
         Perk Property CapCollector01 Auto Const
         Perk Property CapCollector02 Auto Const
         Perk Property CapCollector03 Auto Const
         Perk Property LadyKiller01 Auto Const
         Perk Property LadyKiller02 Auto Const
         Perk Property LadyKiller03 Auto Const
         Perk Property BlackWidow01 Auto Const
         Perk Property BlackWidow02 Auto Const
         Perk Property BlackWidow03 Auto Const
         Perk Property LoneWanderer01 Auto Const
         Perk Property LoneWanderer02 Auto Const
         Perk Property LoneWanderer03 Auto Const
         Perk Property AttackDog01 Auto Const
         Perk Property AttackDog02 Auto Const
         Perk Property AttackDog03 Auto Const
         Perk Property AnimalFriend01 Auto Const
         Perk Property AnimalFriend02 Auto Const
         Perk Property AnimalFriend03 Auto Const
         Perk Property LocalLeader01 Auto Const
         Perk Property LocalLeader02 Auto Const
         Perk Property PartyBoy01 Auto Const
         Perk Property PartyBoy02 Auto Const
         Perk Property PartyBoy03 Auto Const
         Perk Property PartyGirl01 Auto Const
         Perk Property PartyGirl02 Auto Const
         Perk Property PartyGirl03 Auto Const
         Perk Property Inspirational01 Auto Const
         Perk Property Inspirational02 Auto Const
         Perk Property Inspirational03 Auto Const
         Perk Property WastelandWhisperer01 Auto Const
         Perk Property WastelandWhisperer02 Auto Const
         Perk Property WastelandWhisperer03 Auto Const
         Perk Property Intimidation01 Auto Const
         Perk Property Intimidation02 Auto Const
         Perk Property Intimidation03 Auto Const
         Perk Property VANS Auto Const
         Perk Property Medic01 Auto Const
         Perk Property Medic02 Auto Const
         Perk Property Medic03 Auto Const
         Perk Property Medic04 Auto Const
         Perk Property GunNut01 Auto Const
         Perk Property GunNut02 Auto Const
         Perk Property GunNut03 Auto Const
         Perk Property GunNut04 Auto Const
         Perk Property Hacker01 Auto Const
         Perk Property Hacker02 Auto Const
         Perk Property Hacker03 Auto Const
         Perk Property Hacker04 Auto Const
         Perk Property Scrapper01 Auto Const
         Perk Property Scrapper02 Auto Const
         Perk Property Science01 Auto Const
         Perk Property Science02 Auto Const
         Perk Property Science03 Auto Const
         Perk Property Science04 Auto Const
         Perk Property Chemist01 Auto Const
         Perk Property Chemist02 Auto Const
         Perk Property Chemist03 Auto Const
         Perk Property Chemist04 Auto Const
         Perk Property RoboticsExpert01 Auto Const
         Perk Property RoboticsExpert02 Auto Const
         Perk Property RoboticsExpert03 Auto Const
         Perk Property NuclearPhysicist01 Auto Const
         Perk Property NuclearPhysicist02 Auto Const
         Perk Property NuclearPhysicist03 Auto Const
         Perk Property NerdRage01 Auto Const
         Perk Property NerdRage02 Auto Const
         Perk Property NerdRage03 Auto Const
         Perk Property Gunslinger01 Auto Const
         Perk Property Gunslinger02 Auto Const
         Perk Property Gunslinger03 Auto Const
         Perk Property Gunslinger04 Auto Const
         Perk Property Gunslinger05 Auto Const
         Perk Property Commando01 Auto Const
         Perk Property Commando02 Auto Const
         Perk Property Commando03 Auto Const
         Perk Property Commando04 Auto Const
         Perk Property Commando05 Auto Const
         Perk Property Sneak01 Auto Const
         Perk Property Sneak02 Auto Const
         Perk Property Sneak03 Auto Const
         Perk Property Sneak04 Auto Const
         Perk Property Sneak05 Auto Const
         Perk Property MisterSandman01 Auto Const
         Perk Property MisterSandman02 Auto Const
         Perk Property MisterSandman03 Auto Const
         Perk Property ActionBoy01 Auto Const
         Perk Property ActionBoy02 Auto Const
         Perk Property ActionGirl01 Auto Const
         Perk Property ActionGirl02 Auto Const
         Perk Property MovingTarget01 Auto Const
         Perk Property MovingTarget02 Auto Const
         Perk Property MovingTarget03 Auto Const
         Perk Property Ninja01 Auto Const
         Perk Property Ninja02 Auto Const
         Perk Property Ninja03 Auto Const
         Perk Property QuickHands01 Auto Const
         Perk Property QuickHands02 Auto Const
         Perk Property Blitz01 Auto Const
         Perk Property Blitz02 Auto Const
         Perk Property GunFu01 Auto Const
         Perk Property GunFu02 Auto Const
         Perk Property GunFu03 Auto Const
         Perk Property FortuneFinder01 Auto Const
         Perk Property FortuneFinder02 Auto Const
         Perk Property FortuneFinder03 Auto Const
         Perk Property FortuneFinder04 Auto Const
         Perk Property Scrounger01 Auto Const
         Perk Property Scrounger02 Auto Const
         Perk Property Scrounger03 Auto Const
         Perk Property Scrounger04 Auto Const
         Perk Property BloodyMessPerk01 Auto Const
         Perk Property BloodyMessPerk02 Auto Const
         Perk Property BloodyMessPerk03 Auto Const
         Perk Property BloodyMessPerk04 Auto Const
         Perk Property MysteriousStranger01 Auto Const
         Perk Property MysteriousStranger02 Auto Const
         Perk Property MysteriousStranger03 Auto Const
         Perk Property IdiotSavant01 Auto Const
         Perk Property IdiotSavant02 Auto Const
         Perk Property IdiotSavant03 Auto Const
         Perk Property BetterCriticals01 Auto Const
         Perk Property BetterCriticals02 Auto Const
         Perk Property BetterCriticals03 Auto Const
         Perk Property CriticalBanker01 Auto Const
         Perk Property CriticalBanker02 Auto Const
         Perk Property CriticalBanker03 Auto Const
         Perk Property GrimReaperSprint01 Auto Const
         Perk Property GrimReaperSprint02 Auto Const
         Perk Property GrimReaperSprint03 Auto Const
         Perk Property FourLeafClover01 Auto Const
         Perk Property FourLeafClover02 Auto Const
         Perk Property FourLeafClover03 Auto Const
         Perk Property FourLeafClover04 Auto Const
         Perk Property Ricochet01 Auto Const
         Perk Property Ricochet02 Auto Const
         Perk Property Ricochet03 Auto Const
         endGroup


      SoundCategory Property _AudioCategoryMaster Auto Const




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;**********************Test***********************;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;            ; Adds this marker to the map
;            PlacesOfInterest[1].AddtoMap()
;            PlacesOfInterest[2].AddtoMap()
;            PlacesOfInterest[3].AddtoMap()
;            PlacesOfInterest[1].Enable()
;            PlacesOfInterest[2].Enable()
;            PlacesOfInterest[3].Enable()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;**********************Test***********************;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;            








Potion Property BulletTimeConfiguration Auto

LocationAlias Property NewProperty Auto Const

Location Property NewPropertya Auto Const
