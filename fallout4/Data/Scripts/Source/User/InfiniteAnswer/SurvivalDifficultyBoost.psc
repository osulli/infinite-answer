Scriptname InfiniteAnswer:SurvivalDifficultyBoost extends ObjectReference




Event OnRead()
If HasRead == 0
If Game.GetDifficulty() == 6 ;survival

Game.GetXPForLevel(10) - Game.GetXPForLevel(Game.GetPlayerLevel())

Game.GetPlayer().SetValue( Health, 250 )
Game.GetPlayer().AddPerk(Toughness01)
Game.GetPlayer().AddPerk(LifeGiver01)
Game.GetPlayer().AddPerk(Medic01)
Game.GetPlayer().Additem(Stimpak, 5 )

HasRead = 1

EndIf
EndIf
endEvent


Potion Property Stimpak Auto Const
ActorValue Property Health Auto Const
Perk Property Toughness01 Auto Const
Perk Property LifeGiver01 Auto Const
Perk Property Medic01 Auto Const
Bool Property HasRead Auto



