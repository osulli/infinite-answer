Scriptname InfiniteAnswer:ReadPostGlitchNote extends ObjectReference Const


Event OnRead()

;If HasOptions == 0	

 ;Quests
MQ102.SetStage(10) ;MQ102 "Return Home"


Game.GetPlayer().Additem(OSM_AlternateStart_Options, 1, false)	;Additem InfiniteAnswerOptions

;HasOptions.SetValueInt(1)
;EndIf

endEvent


Holotape Property OSM_AlternateStart_Options Auto Const


;GlobalVariable Property HasOptions Auto

Quest Property MQ102 Auto Const

Quest[] Property QuestRefs Auto Const
