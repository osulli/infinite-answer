;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:TERM_OSM_QuestSettings Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
;add nick as companion by finishing his quest
(MQ105 as Fragments:Quests:QF_MQ105_000229E6).Alias_NickValentine.GetActorRef().SetAvailableToBeCompanion()
(MQ105 as Fragments:Quests:QF_MQ105_000229E6).Alias_NickValentine.GetActorRef().EvaluatePackage()
(MQ105 as Fragments:Quests:QF_MQ105_000229E6).Alias_NickValentine.GetActorRef().moveto(MQ105NickOutsideHisOfficeMarker)

;start far harbor
bool FarHarborInstalled = Game.IsPluginInstalled("DLCCoast.esm")
if FarHarborInstalled == true

  		Quest DLC03MQ01FarFromHome = Game.GetFormFromFile(0x001b3f, "DLCCoast.esm") as Quest
  		DLC03MQ01FarFromHome.SetStage(5)
 Elseif  FarHarborInstalled == false
 		debug.notification("You do not have Far Harbor installed")
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
;start automatron

bool AutomatronInstalled = Game.IsPluginInstalled("DLCRobot.esm")
if AutomatronInstalled == true
  		Quest DLC01MQ01MechanicalMenace = Game.GetFormFromFile(0x000806, "DLCRobot.esm") as Quest
  		DLC01MQ01MechanicalMenace.SetStage(10)
 Elseif  AutomatronInstalled == false
 		debug.notification("You do not have Automatron installed")
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property OSM_101 Auto Const

Quest Property MQ105 Auto Const

ReferenceAlias Property DiamondCityValentinesLocation Auto Const

ObjectReference Property MQ105NickOutsideHisOfficeMarker Auto Const
