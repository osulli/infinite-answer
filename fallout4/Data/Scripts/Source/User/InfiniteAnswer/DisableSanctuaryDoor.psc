Scriptname InfiniteAnswer:DisableSanctuaryDoor extends ObjectReference 

ObjectReference Property SanctuaryDoor Auto Const

Bool Property TriggerEntered Auto

Event OnTriggerEnter(ObjectReference akActionRef)

If TriggerEntered == false
	TriggerEntered = true
  	SanctuaryDoor.Disable()
  	Game.RequestAutoSave()
  Endif
EndEvent
