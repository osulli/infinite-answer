;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:TERM_PluginInstalledTest Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
;Load the Protocol
bool EnclaveX02ArmorInstalled = Game.IsPluginInstalled("EnclaveX02.esp")
if EnclaveX02ArmorInstalled == true
  ;game.getplayer.additem(FullSetOfX02)
  debug.notification("You do have EnclaveX02.esp installed")
 Elseif  EnclaveX02ArmorInstalled == false
 		debug.notification("You do not have EnclaveX02.esp installed")
endIf


;(OSM_101 as InfiniteAnswer:OSM_LoadProtocol).ProtocolIndex = 69
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
;(OSM_101 as InfiniteAnswer:OSM_LoadProtocol).LoadModFixes()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property OSM_101 Auto Const

