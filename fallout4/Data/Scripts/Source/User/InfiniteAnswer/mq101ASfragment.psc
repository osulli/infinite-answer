;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:mq101ASfragment Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0000_Item_00
Function Fragment_Stage_0000_Item_00()
;BEGIN AUTOCAST TYPE InfiniteAnswer:OSM_MQ101QuestScript
Quest __temp = self as Quest
InfiniteAnswer:OSM_MQ101QuestScript kmyQuest = __temp as InfiniteAnswer:OSM_MQ101QuestScript
;END AUTOCAST
;BEGIN CODE
;;osm

;Store actors for optimization
Actor PlayerREF = Game.GetPlayer()
Actor SpouseMaleREF = Alias_SpouseMale.GetActorRef()
Actor SpouseFemaleREF = Alias_SpouseFemale.GetActorRef()

kmyquest.MQ101EnableLayer = InputEnableLayer.Create()
kmyquest.MQ101EnableLayer.DisablePlayerControls()

;set charGen skeletons
PlayerREF.SetHasCharGenSkeleton()
SpouseFemaleREF.SetHasCharGenSkeleton()

;precache the face gen data
Game.PrecacheCharGen()

; move the player so that New Game button puts him in the right place
PlayerREF.MoveTo(pMQ101PlayerStartMarker01)

; setup spouse
Alias_ActiveSpouse.ForceRefTo(SpouseFemaleREF)

;put player camera in sink
Alias_FaceGenSink.GetRef().Activate(PlayerREF, abDefaultProcessingOnly=True)

; give the load a second to get the player in the furniture
Utility.Wait(1.0)

;;;;CharGenMirrorImod.Apply() ;what does this do?


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Resetclock
TimeScale.SetValueInt(20)

;standard HUD mode
Game.SetCharGenHUDMode(0)

;allow saving
Game.SetInCharGen(False, False, False)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; give the player the right starting gear
PlayerREF.UnequipItem(Armor_Vault111_Underwear,  absilent=true)
PlayerREF.RemoveItem(Armor_Vault111_Underwear, absilent=true)
PlayerREF.AddItem(Armor_Vault111Clean_Underwear, absilent=true)
PlayerREF.AddItem(Clothes_InstituteUniform, absilent=true)
PlayerREF.EquipItem(Clothes_InstituteUniform, absilent=true)
PlayerREF.AddItem(Pipboy, absilent=true)
PlayerREF.EquipItem(Pipboy,  absilent=true)


; UnMute all sounds(which should still be in synch with their animations.)
_AudioCategoryMaster.UnMute()
; Restart sounds.
_AudioCategoryMaster.UnPause()


; fade the game in
Game.FadeOutGame(False, False, 1.0, 2.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0000_Item_02
Function Fragment_Stage_0000_Item_02()
;BEGIN CODE
;fuck you thief
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0000_Item_03
Function Fragment_Stage_0000_Item_03()
;BEGIN CODE
;fuck you thief
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN AUTOCAST TYPE InfiniteAnswer:OSM_MQ101QuestScript
Quest __temp = self as Quest
InfiniteAnswer:OSM_MQ101QuestScript kmyQuest = __temp as InfiniteAnswer:OSM_MQ101QuestScript
;END AUTOCAST
;BEGIN CODE
;push sound category for interior
CSMQ101PlayerHouseInt.Push(4.0)

;don't allow the player to open the door yet
Alias_PlayerHouseDoor.GetRef().BlockActivation(True, True)

;turn on the radio
pRadioSanctuaryHillsPrewar.Start()
Alias_RadioTransmitter.GetRef().Enable()

;turn off sink soundFX
CharGenSinkSoundFXEnableMarker.Disable()

;player can only fast walk
Game.GetPlayer().ChangeAnimArchetype(AnimArchetypeFastWalk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0012_Item_00
Function Fragment_Stage_0012_Item_00()
;BEGIN AUTOCAST TYPE InfiniteAnswer:OSM_MQ101QuestScript
Quest __temp = self as Quest
InfiniteAnswer:OSM_MQ101QuestScript kmyQuest = __temp as InfiniteAnswer:OSM_MQ101QuestScript
;END AUTOCAST
;BEGIN CODE
Alias_ActiveSpouse.GetActorRef().EvaluatePackage()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0015_Item_00
Function Fragment_Stage_0015_Item_00()
;BEGIN AUTOCAST TYPE InfiniteAnswer:OSM_MQ101QuestScript
Quest __temp = self as Quest
InfiniteAnswer:OSM_MQ101QuestScript kmyQuest = __temp as InfiniteAnswer:OSM_MQ101QuestScript
;END AUTOCAST
;BEGIN CODE
;start checking when the player switches gender so appropriate spouse plays idle lines
kmyquest.TrackMirrorGenderSwitch()

;player must start facegen in neutral
Game.GetPlayer().ChangeAnimFaceArchetype(AnimFaceArchetypeNeutral)

;start headtrack targets
;Game.GetPlayer().SetLookat(MQ101MirrorMarker)
;Alias_ActiveSpouse.GetActorRef().SetLookAT(MQ101MirrorMarker)

;also start tracking for menu exit
kmyquest.RegisterForAnimationEvent(Game.GetPlayer(), "ChargenSkeletonReset")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0018_Item_00
Function Fragment_Stage_0018_Item_00()
;BEGIN AUTOCAST TYPE InfiniteAnswer:OSM_MQ101QuestScript
Quest __temp = self as Quest
InfiniteAnswer:OSM_MQ101QuestScript kmyQuest = __temp as InfiniteAnswer:OSM_MQ101QuestScript
;END AUTOCAST
;BEGIN CODE
pMQ101_001_MirrorScene.Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property CharGenPlayerMarker1  Auto Const  
MQ03QuestScript Property MQ03 Auto Const

Armor Property pArmor_Vault111_Underwear Auto Const

ObjectReference Property pMQPlayerCryopodREF Auto Const

Armor Property pPipboyDusty Auto Const

Armor Property pArmor_WeddingRing Auto Const

ImageSpaceModifier Property pCryoWakeImod Auto Const

ObjectReference Property pMQ101PlayerStartMarker01 Auto Const

ReferenceAlias Property Alias_SpouseMale Auto Const

ReferenceAlias Property Alias_SpouseFemale Auto Const

ReferenceAlias Property Alias_ActiveSpouse Auto Const

Scene Property pMQ101_001_MirrorScene Auto Const

ObjectReference Property pMQ101SpouseStartMarker Auto Const

Scene Property pMQ101_002_CodsworthScene01 Auto Const

ReferenceAlias Property Alias_CodsworthPrewar Auto Const

ReferenceAlias Property Alias_PlayerHouseDoor Auto Const

ReferenceAlias Property Alias_VaultTecRep Auto Const

Scene Property pMQ101_004_ShaunCries Auto Const

Scene Property pMQ101_005_Doorbell Auto Const

ReferenceAlias Property Alias_RadioTransmitter Auto Const

Quest Property pRadioSanctuaryHillsPrewar Auto Const

Quest Property pMQ101TVStation Auto Const

Scene Property pMQ101_008_AfterSign Auto Const

Scene Property pMQ101_010_AfterNukeTV Auto Const

Scene Property pMQ101_009_GotoShaun Auto Const

ObjectReference Property pMQ101SpouseToBabyRoomMarker Auto Const

ObjectReference Property pMQ101BabyRoomDoorCollisionREF Auto Const

ReferenceAlias Property Alias_CribMobile Auto Const

Scene Property pMQ101_010_SpouseMobile Auto Const

Scene Property pMQ101_011_MobileSpin Auto Const

ObjectReference Property pMQ101SpouseInsideBabyRoomMarker Auto Const

ReferenceAlias Property Alias_BabyRoomDoor Auto Const

ReferenceAlias Property Alias_ShaunCrib Auto Const

ObjectReference Property pMQ101SoldiersEnableMarker Auto Const

ObjectReference Property pMQ101FrontDoorCollisionREF Auto Const

ObjectReference Property pMQ101VaultTecRepToVaultMarker Auto Const

Scene Property pMQ101_013_EmergencyBroadcast Auto Const

Quest Property pMQ101PrewarSanctuaryHills Auto Const

Scene Property pMQ101_016_VaultTecRepSoldierScene Auto Const

ReferenceAlias Property Alias_ExtVaultElevator Auto Const

Scene Property pMQ101_017_ListCheckingScene Auto Const

ReferenceAlias Property Alias_ExtElevatorButton Auto Const

Scene Property pMQ101_018_ElevatorDescendScene Auto Const

ObjectReference Property pPrewarVaultElevatorCollisionEnableMarker Auto Const

ObjectReference Property pPrewarVaultIntAutoloadDoor Auto Const

ObjectReference Property pPrewarVaultElevatorREF Auto Const

ReferenceAlias Property Alias_NeighborWindowRunner Auto Const

ObjectReference Property pMQ101SpouseElevatorMarker02 Auto Const

Scene Property pMQ101_019_Vault111Greeting Auto Const

ReferenceAlias Property Alias_VaultTecGreeter Auto Const

ReferenceAlias Property Alias_V111LockedDoor Auto Const

ReferenceAlias Property Alias_SpousePod Auto Const

ReferenceAlias Property Alias_PlayerPod Auto Const

Scene Property pMQ101_020_Vault11ToPods Auto Const

ReferenceAlias Property Alias_Solider_ListChecker Auto Const

ReferenceAlias Property Alias_Kellogg Auto Const

ReferenceAlias Property Alias_InstScientistFemale Auto Const

ReferenceAlias Property Alias_InstScientistMale Auto Const

Scene Property pMQ101_022_FreezingSequence Auto Const

ReferenceAlias Property Alias_MrAble Auto Const

ReferenceAlias Property Alias_MrsAble Auto Const

ObjectReference Property pMQ101MrAbleElevatorMarker02 Auto Const

ObjectReference Property pMQ101MrsAbleElevatorMarker02 Auto Const

Quest Property pMQ101Vault111 Auto Const

Quest Property pMQ101KelloggSequence Auto Const

Quest Property pMQ101SanctuaryHills Auto Const

ReferenceAlias Property Alias_VaultTecEscort Auto Const

ObjectReference Property pMQ101AirRaidSirenMarkerREF Auto Const

sound Property pFXExplosionNukeChargenA Auto Const

sound Property pFXExplosionNukeChargenB Auto Const

ObjectReference Property pMQ101CrowdPanicSoundMarkerREF Auto Const

Armor Property pClothesSweaterVest Auto Const

Armor Property pClothesPreWarDress Auto Const

Quest Property pMQ101PlayerComments Auto Const

ObjectReference Property pMQ101PlayerKellogSequenceMarker Auto Const

ObjectReference Property pMQ203PlayerCryopodREF Auto Const

Outfit Property pMQ101SpouseFemaleBabyOutfit Auto Const

Outfit Property MQ101SpouseMaleBabyOutfit Auto Const

Keyword Property AnimFlavorHoldingBaby Auto Const

Weather Property PrewarPlayerHouseInteriorWeather Auto Const

Weather Property CommonwealthClear Auto Const

ObjectReference Property MQ101CribMusicREF Auto Const

ReferenceAlias Property Alias_CribStatic Auto Const

ObjectReference Property MQ101VertibirdsEnableMarker Auto Const

ReferenceAlias Property Alias_PlayerCribAnim Auto Const

Armor Property BabyBundled Auto Const

Keyword Property AnimFaceArchetypeNervous Auto Const

Keyword Property AnimFaceArchetypePlayer Auto Const

ObjectReference Property MQ101PlayerSkipToExteriorMarker Auto Const

ObjectReference Property MQ101VertibirdBEnableMarker Auto Const

ObjectReference Property MQ101RunnersEnableMarker Auto Const

ObjectReference Property MQ101StreetSoldiersEnableMarker Auto Const

ObjectReference Property MQ101StreetNeighborsEnableMarker Auto Const

Keyword Property AnimFlavorClipboard Auto Const

ObjectReference Property MQ101VaultGateCollisionREF Auto Const

ObjectReference Property MQ101VertibirdCEnableMarker Auto Const

ObjectReference Property CG_NukeFXControlMarker Auto Const

Scene Property MQ101_018_NukeExplodes Auto Const

SoundCategorySnapshot Property CSMQ101PlayerHouseInt Auto Const

ObjectReference Property ShaunBabyAudioRepeaterActivator Auto Const

ObjectReference Property ShaunIdleSoundMarkerREF Auto Const

ObjectReference Property MQ101PlayerElevatorMarker Auto Const

ReferenceAlias Property Alias_VaultIntElevator Auto Const

ObjectReference Property MQ101HillSoldiersEnableMarker Auto Const

ReferenceAlias Property Alias_MrWhitfield Auto Const

ReferenceAlias Property Alias_MrsWhitfield Auto Const

ReferenceAlias Property Alias_MrRussell Auto Const

ObjectReference Property MQ101MrRussellElevatorMarker02 Auto Const

ObjectReference Property MQ101MrWhitefieldElevatorMarker02 Auto Const

ObjectReference Property MQ101MrsWhitefieldElevatorMarker02 Auto Const

Scene Property MQ101_020_Vault111Suit Auto Const

Outfit Property MQ101SpouseVaultSuitBabyOutfit Auto Const

sound Property QSTChargenVaultSuitOn Auto Const

ObjectReference Property PrewarVault111MagicDoor Auto Const

ObjectReference Property MQ101Vault111PreloadPlayerMoveMarker Auto Const

ObjectReference Property MQ101KelloggSeqMovePlayerForPreload Auto Const

ObjectReference Property MQ203Vault111MagicDoorToPostWar Auto Const

Quest Property MQ102 Auto Const

Armor Property ClothesPrewarTshirtSlacks Auto Const

Armor Property ClothesPrewarWomensCasual Auto Const

ReferenceAlias Property Alias_MrCallahanPod Auto Const

ReferenceAlias Property Alias_MrsCallahanPod Auto Const

Keyword Property AnimFlavorBombReaction Auto Const

VisualEffect Property CameraAttachGroggySleep01FX Auto Const

ImageSpaceModifier Property CryoSleepImod Auto Const

VisualEffect Property CameraAttachGroggyWake01FX Auto Const

ImageSpaceModifier Property CryoWakeImod Auto Const

ImageSpaceModifier Property CryoSuspensionImod Auto Const

Keyword Property IsInCryopodAwake Auto Const

ReferenceAlias Property Alias_MQ203PlayerCryopod Auto Const

Armor Property ChargenPlayerClothes Auto Const

Scene Property MQ101_020c_Vault111PodsSet Auto Const

ReferenceAlias Property Alias_ShaunName Auto Const

Message Property MQ101ShaunActivationText Auto Const

Scene Property MQ101_020b_Vault111Shaun Auto Const

Keyword Property AnimFlavorBombReactionBaby Auto Const

Scene Property MQ101_020a_Vault111ToPods Auto Const

ReferenceAlias Property Alias_FaceGenSink Auto Const

Idle Property HandyEquipDuster Auto Const

Idle Property HandyUnequipDuster Auto Const

ObjectReference Property MQ203Vault111MagicDoorToPreWar Auto Const

ObjectReference Property MQ101Vault111PostWarMagicDoor Auto Const

Scene Property MQ101_019a_Vault111Greeting02 Auto Const

Scene Property MQ101_019a_Vault111Greeting01b Auto Const

Scene Property MQ101_019a_Vault111Greeting01a Auto Const

Scene Property MQ101_019a_Vault111PA Auto Const

Scene Property MQ101_019a_Vault111Greeting03 Auto Const

ReferenceAlias Property Alias_NeighborCryopod01 Auto Const

ReferenceAlias Property Alias_NeighborCryopod02 Auto Const

ReferenceAlias Property Alias_NeighborCryopod03 Auto Const

ReferenceAlias Property Alias_NeighborCryopod04 Auto Const

ObjectReference Property MQ101ShaunRoomCollisionEnableMArker Auto Const

ReferenceAlias Property Alias_BabyActivator Auto Const

GlobalVariable Property TimeScale Auto Const

Scene Property MQ101_005_DoorbellRepeat Auto Const

Keyword Property AnimFaceArchetypeFriendly Auto Const

Quest Property MQ101Radio Auto Const

ObjectReference Property MQ101SpouseEscortMarker01 Auto Const

ObjectReference Property MQ101SpouseFailsafeLookAtMarker Auto Const

ObjectReference Property MQ101SpouseFailsafeMoveMarker Auto Const

ReferenceAlias Property Alias_VaultTecSuitGiver Auto Const

ObjectReference Property MQ101Vault111SpouseEscortMarker01 Auto Const

ObjectReference Property MQ101ElevatorCenterMark Auto Const

Quest Property MQ101SanctuaryHills Auto Const

ObjectReference Property MQ101MirrorMarker Auto Const

ObjectReference Property MQ101CodsworthStartMarker Auto Const

ObjectReference Property MQ101SpouseGateFailsafeMarker Auto Const

ReferenceAlias Property Alias_VaultTecElevatorEscort Auto Const

MusicType Property MUSSpecialChargenNukeA Auto Const

MusicType Property MUSSpecialChargenNukeB Auto Const

ObjectReference Property MQ101GuardElevatorMarker Auto Const

SoundCategorySnapshot Property CSMQ101Cryopod Auto Const

sound Property QSTPlayerCryopodGas Auto Const

ImageSpaceModifier Property CharGenCameraImod Auto Const

MusicType Property MUSSpecialChargenRunForTheVault Auto Const

ObjectReference Property CgNukeShockWaveRef Auto Const

Idle Property IdleLookAtOwnOutfit Auto Const

Idle Property HandyUnequipCoffeePot Auto Const

Keyword Property AnimArchetypeShocked Auto Const

LocationAlias Property Alias_PrewarVault111Location Auto Const

Keyword Property AnimArchetypeScared Auto Const

Outfit Property Vault111SuitNoPipboy Auto Const

Scene Property MQ101_016_VaultTecRepSoldierScene Auto Const

Armor Property Armor_Vault111Clean_Underwear Auto Const

Outfit Property MQ101SpouseVaultSuitBabyOutfit_Clean Auto Const

LocationAlias Property Alias_MQ203Location Auto Const

ReferenceAlias Property Alias_ShaunActivateTriggerBeforeScene Auto Const

ReferenceAlias Property Alias_VaultTecScientistF03Cryo Auto Const

ReferenceAlias Property Alias_VaultTecScientistM05Cryo Auto Const

Keyword Property AnimArchetypeFastWalk Auto Const

Keyword Property AnimArchetypePlayer Auto Const

Scene Property MQ101_016b_SoldierGateLoop Auto Const

Scene Property MQ101_006_VaultTecRep Auto Const

Quest Property MQ101Vault111 Auto Const

Idle Property FlinchChargenA Auto Const

Idle Property FlinchChargenB Auto Const

Idle Property FlinchChargenC Auto Const

Idle Property IdleCryopodWaveToSpouse Auto Const

LocationAlias Property Alias_PrewarSanctuaryHillsLocation Auto Const

ImageSpaceModifier Property CharGenMirrorImod Auto Const

Sound Property AMBIntChargenPlayerHouseBathroomSpotMirrorWipe Auto Const

ObjectReference Property CharGenSinkSoundFXEnableMarker Auto Const

ObjectReference Property MQ101SpouseLeanREF Auto Const

Keyword Property AnimFlavorClipboardSalesman Auto Const

Armor Property Armor_Vault111_Underwear Auto Const

Keyword Property AnimFaceArchetypeConfident Auto Const Mandatory

Keyword Property AnimFaceArchetypeNeutral Auto Const Mandatory

ReferenceAlias Property Alias_PlayerFrontDoorCloset Auto Const Mandatory

ObjectReference Property MQ101ShaunRoomCollisionEnableMarker002 Auto Const Mandatory

ReferenceAlias Property Alias_SpouseFemaleName Auto Const Mandatory

ReferenceAlias Property Alias_SpouseNameMale Auto Const Mandatory

ReferenceAlias Property Alias_SpouseFemaleNamePermanent Auto Const Mandatory

ReferenceAlias Property Alias_SpouseNameMalePermanent Auto Const Mandatory

Idle Property IdleStop Auto Const Mandatory

Scene Property MQ101_007_PlayerDoesntSign Auto Const Mandatory

Armor Property Clothes_InstituteUniform Auto Const

Armor Property Pipboy Auto Const

SoundCategory Property _AudioCategoryMaster Auto Const

Outfit Property OSM_InstituteOutfit Auto Const
