;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname InfiniteAnswer:TERM_OSM_AlternateStart_Term_01000F9D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
;Load Random Protocol
; Get a random number between 0 and NumberOfProtocols
(OSM_AlternateStart_Quest as OSM_LoadProtocol).ProtocolIndex = Utility.RandomInt(1, 25)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment


Quest Property OSM_AlternateStart_Quest Auto Const
