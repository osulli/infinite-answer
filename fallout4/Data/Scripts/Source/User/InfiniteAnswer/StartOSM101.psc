Scriptname InfiniteAnswer:StartOSM101 extends ObjectReference

Quest Property OSM_101 Auto Const
Bool Property HasQuest Auto


Event OnTriggerEnter(ObjectReference akActionRef)
If HasQuest == False
	OSM_101.SetStage(10) 
	HasQuest = True
EndIf 

EndEvent


