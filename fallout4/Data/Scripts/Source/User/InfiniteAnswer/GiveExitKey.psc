Scriptname InfiniteAnswer:GiveExitKey extends ObjectReference Const

Key Property OSM_AlternateCellKey Auto Const
Quest Property OSM_101 Auto Const



Event OnActivate(ObjectReference akActionRef)
OSM_101.SetStage(20)
  Game.GetPlayer().Additem(OSM_AlternateCellKey, 1 , true)

  DoorToNextArea.disable()
EndEvent






ObjectReference Property DoorToNextArea Auto Const

