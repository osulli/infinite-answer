Scriptname InfiniteAnswer:OpenRailroadSecretEntrance extends ObjectReference




Event OnTriggerEnter(ObjectReference akActionRef)

If TriggerEntered == false
	TriggerEntered = true
	RailroadSecretDoor.SetOpen()

  Endif
EndEvent






ObjectReference Property RailroadSecretDoor Auto Const


Bool Property TriggerEntered Auto