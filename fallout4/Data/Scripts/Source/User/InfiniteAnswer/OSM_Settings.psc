Scriptname InfiniteAnswer:OSM_Settings extends Quest 
Function ApplyFixes()




		;;;;;;;;;;;;;;;;;;;;;**********************************************************************************************************************************************************************************************;;;;;;;;;;;;;;;;;;;;;	
		;;;;;;;;;;;;;;;;;;;;;*******************if you want to change any of the settings before, you need to make that a 'new fix' cos once that bool is set to true it won't run again*******************;;;;;;;;;;;;;;;;;;;;;
		;;;;;;;;;;;;;;;;;;;;;**********************************************************************************************************************************************************************************************;;;;;;;;;;;;;;;;;;;;

 		If V185Applied == false
;;;  	 			V185Applied = true


       		If Game.GetPlayer().GetRace() == GhoulRace
       		    Game.GetPlayer().SetRace(GhoulRace)
       		EndIf

  			Debug.Notification("v1.8.5 Retroactive Fixes applied.")


		EndIf  	
		Debug.Notification("All possible fixes have been applied")
		EndFunction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Function CompleteQuestsHunterHunted()
	
	If HunterHuntedCompleted == false
	    MQ102.setstage(150)
        MQ102.setstage(200)
        MQ103.setstage(100)
        MQ104.setstage(50)
        MQ104.setstage(110)
        MQ104.setstage(120)
        MQ104.setstage(210)
        MS07Intro.setstage(10)
        MS07Intro.setstage(100)
        MS07Intro.setstage(200)
        MS07Intro.setstage(205)
        MS07Intro.setstage(400)

        MQ105.setstage(50)
        MQ105.setstage(60)
        MQ105.setstage(62)
        MQ105.setstage(100)
        MQ105.setstage(300)
        MQ105.setstage(410)
 ;;;       MQ105.setstage(415) ;moves dogmeat
        MQ105.setstage(600)
        MQ105.setstage(1000)

        MQ106.setstage(10)
        MQ106.setstage(40)
        MQ106.setstage(41)
        MQ106.setstage(900) ;kill kellog, enable airport, fort hagen set cleared 
        MQ106.setstage(1000) 
        MQ106.setstage(1150)
        MQ106.setstage(2000)

        MQ201.setstage(1000)
        MQ202.setstage(310)
        MQ202.setstage(320)
        MQ203.setstage(1100) ;debug stage (might not be needed?)
        MQ204.setstage(5) ;activates Virgil
        MQ204.setstage(200) ;Player will start on Hunter/Hunted
   		Debug.MessageBox("Quests up to Hunter/Hunted have been completed")

        HunterHuntedCompleted = true
        EndIf
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	EndFunction




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Bool Property V185Applied Auto
Bool Property HunterHuntedCompleted Auto
Bool Property SpareBoolOSM Auto
Race Property GhoulRace Auto


Group Quests
         Quest Property MQ101 Auto Const
         Quest Property MQ102 Auto Const
         Quest Property MQ103 Auto Const
         Quest Property MQ104 Auto Const
         Quest Property MQ105 Auto Const
         Quest Property MQ106 Auto Const
         Quest Property MQ201 Auto Const
         Quest Property MQ202 Auto Const
         Quest Property MQ203 Auto Const
         Quest Property MQ204 Auto Const

         
         Quest Property MQ205 Auto Const
         Quest Property MQ206 Auto Const
         Quest Property MQ207 Auto Const


         Quest Property BoSDialoguePrydwen Auto Const ;use this to set player BoS rank
         Quest Property BoSEnable Auto Const ;use this to set whether Prydwen/Liberty etc are active

         Quest Property BoS000 Auto Const
         Quest Property BoS100 Auto Const
         Quest Property BoS101 Auto Const
         Quest Property BoS200 Auto Const
         Quest Property BoS201 Auto Const
         Quest Property BoS201B Auto Const
         Quest Property BoS202 Auto Const
         Quest Property BoS203 Auto Const
         Quest Property BoS204 Auto Const
         Quest Property BoS301 Auto Const
         Quest Property BoS302 Auto Const
         Quest Property BoS302B Auto Const
         Quest Property BoS303 Auto Const
         Quest Property BoS304 Auto Const
         Quest Property MQ302BoS Auto Const
         Quest Property BoS305 Auto Const

         Quest Property RR101 Auto Const
         Quest Property RR102 Auto Const
         Quest Property RR201 Auto Const
         Quest Property RR302 Auto Const
         Quest Property RR303 Auto Const
         Quest Property MQ302RR Auto Const


         Quest Property Inst301 Auto Const
         Quest Property Inst302 Auto Const
         Quest Property Inst303 Auto Const
         Quest Property InstMassFusion Auto Const
         Quest Property MinVsInst Auto Const
         Quest Property Inst305 Auto Const
         Quest Property Inst306 Auto Const
         Quest Property Inst307 Auto Const
         Quest Property Inst308 Auto Const
         Quest Property InstKickOut Auto Const

         Quest Property Min00 Auto Const
         Quest Property MinRecruit00 Auto Const ;RECRUIT PRESTON
         Quest Property Min02 Auto Const
         Quest Property Min03 Auto Const
         Quest Property Min207 Auto Const
         Quest Property Min301 Auto Const
         Quest Property MinDefendCastle Auto Const
         Quest Property MQ302Min Auto Const
         Quest Property MinDestBoS Auto Const


         Quest Property RadioDiamondCity Auto Const
         Quest Property RadioInstituteQuest Auto Const
         Quest Property DialogueDiamondCityEntrance Auto Const
         Quest Property FFDiamondCity07 Auto Const
         Quest Property MS07 Auto Const
         Quest Property MS07Intro Auto Const



         


         endGroup
