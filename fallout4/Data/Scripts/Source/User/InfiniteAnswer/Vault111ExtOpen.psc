Scriptname InfiniteAnswer:Vault111ExtOpen extends ObjectReference


ObjectReference Property Vault111ExtDoor Auto Const

Bool Property TriggerEntered Auto

Event OnTriggerEnter(ObjectReference akActionRef)

If TriggerEntered == false
	TriggerEntered = true

	Vault111ExtDoor.PlayAnimation("stage5") ;play the ani
	Vault111ElevatorHandler.EnableControlsForElevator()
	V111ElevatorCollisionPlatformEnableMarker.Disable()
  Endif
EndEvent

Vault111ElevatorHandlerScript Property Vault111ElevatorHandler Auto Const
ObjectReference Property V111ElevatorCollisionPlatformEnableMarker Auto Const